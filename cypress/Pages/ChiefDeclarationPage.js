/* eslint-disable no-undef */
/* eslint-disable class-methods-use-this */

/**
 * @author Zeeshan Asghar
 * Dated: 28/10/2021
 */

class ChiefDeclaration {
  // Enter Reference details
  geCreateDeclarationButton() {
    return cy.xpath("//span[normalize-space()='Create Declaration']");
  }

  getSearchEntryReference() {
    return cy.xpath("//input[@id='entryRef_input']");
  }

  getSearchButton() {
    return cy.get("#search_btn");
  }

  getDeclarationStatus() {
    return cy.xpath("//tr[@class='mat-row cdk-row ng-star-inserted']/td[9]/span");
  }

  getDeclarationEntryReference() {
    return cy.xpath("//tr[@class='mat-row cdk-row ng-star-inserted']/td[1]/span");
  }

  getActionsButton() {
    return cy.xpath("//mat-icon[normalize-space()='more_vert']");
  }

  getCancelMenuItem() {
    return cy.xpath("(//span[contains(text(),'Cancel')])[1]");
  }

  getDraftCancelMenuItem() {
    return cy.xpath("(//span[contains(text(),'Cancel')])[2]");
  }

  getCancellationReasonTextArea() {
    return cy.xpath("//textarea");
  }

  getSubmitButton() {
    return cy.get("#queryDialog_submit");
  }

  getCancelSubmitButton() {
    return cy.xpath("//span[normalize-space()='SUBMIT']");
  }

  getDeleteButton() {
    return cy.xpath("//span[normalize-space()='DELETE']");
  }

  getQueryButton() {
    return cy.xpath("//span[normalize-space()='Query']");
  }

  getViewButton() {
    return cy.xpath("//span[normalize-space()='View']");
  }

  getSelectQueryButton() {
    return cy.get("mat-dialog-container #queryDialog_select");
  }

  getDeclrationDataTable() {
    return cy.get("#notFound_td");
  }

  getQueryType(queryType) {
    return cy.xpath(`//span[normalize-space()='${queryType}']`);
  }

  getActionButton() {
    return cy.get("app-declaration-actions button");
  }

  getAmendButton() {
    return cy.get(".cdk-overlay-pane button").eq(3);
  }

  getStatusField() {
    return cy.get("app-custom-input #status_customInput");
  }

  getShowMoreButton() {
    return cy.get("#show_moreBtn");
  }

  // Locator for Add Party
  getAddPartyButton() {
    return cy.get("#add-btn");
  }

  /**
   * clicks on create declration button on chief dashboard
   */
  clickCreateDeclaration() {
    this.geCreateDeclarationButton().click({ force: true });
  }

  /**
   * type entry reference
   */
  typeEntryReference() {
    cy.readFile("cypress/data-files/dynamic-data/dynamic-data.json").then((obj) => {
      const dynamicData = obj;
      this.getSearchEntryReference().clear();
      this.getSearchEntryReference().type(dynamicData.entryReference);
    });
  }

  /**
   * clicks on search button on chief dashboard
   */
  clickSearchButton() {
    this.getSearchButton().click();
  }

  /**
   * clicks on actions button on chief dashboard
   */
  clickActionsButton() {
    this.getActionsButton().click();
  }

  /**
   * clicks on cancel menu on chief dashboard
   */
  clickCancelMenuItem() {
    this.getCancelMenuItem().click();
  }

  /**
   * clicks on cancel declaration menu item to cancel draft
   */
  clickCancelDraftenuItem() {
    this.getDraftCancelMenuItem().click();
  }

  /**
   * type on cancellation reason
   */
  typeCancellationReason(cancellationReason) {
    this.getCancellationReasonTextArea().type(cancellationReason);
  }

  /**
   * clicks on search button on chief dashboard
   */
  clickSubmitButton() {
    this.getSubmitButton().click();
  }

  /**
   * clicks on search button on chief dashboard
   */
  clickCancelSubmitButton() {
    this.getCancelSubmitButton().click();
  }

  /**
   * clicks on delete button to delete draft
   */
  clickDeleteButton() {
    this.getDeleteButton().click();
  }

  /**
   * clicks on query button
   */
  clickQueryMenuItem() {
    this.getQueryButton().click();
  }

  /**
   * clicks on select query button
   */
  clickSelectQueryButton() {
    this.getSelectQueryButton().click({ force: true });
  }

  /**
   * select query type
   */
  selectQueryType(queryType) {
    this.getQueryType(queryType).click();
  }

  /**
   * click view button
   */
  clickViewMenuItem() {
    this.getViewButton().click();
  }

  /**
   * clicks on Amend button on chief dashboard
   */
  clickAmendButton() {
    this.getActionButton().click();
    this.getAmendButton().click();
  }

  /**
   * clicks on show more button on chief dashboard
   */
  clickShowMoreButton() {
    this.getShowMoreButton().click();
  }

  /**
   * type status on chief dashboard
   */
  typeStatus(status) {
    this.getStatusField().type(status).type("{enter}");
  }

  // Code for Create Party 
  /**
   * clicks on create declration button on chief dashboard
   */
   clickAddPartyButton() {
    this.getAddPartyButton().click();
  }

}
export default ChiefDeclaration;
