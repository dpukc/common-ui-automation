/* eslint-disable no-undef */
/* eslint-disable class-methods-use-this */

/**
 * @author Zeeshan Asghar
 * Dated: 28/10/2021
 */

class Header {
  /**
   * Hovers on Manage link in menu
   */
  clickManage() {
    // cy.get("#dtNavbar a[data-i18n='menuManage']").realHover();
    cy.xpath("//a[normalize-space()='Manage']").eq(0).invoke("show").click({ force: true });
  }

  /**
   * clicks on chief declaration in menu
   */
  clickChiefDeclarations() {
    // cy.xpath("//div[@id='dtNavbar']//ul//a[normalize-space()='CHIEF Declarations']").click();
    cy.xpath("//a[normalize-space()='CHIEF Declarations']").eq(0).invoke("show").click({ force: true });
  }

/**
 * click on transit declaration menu option
 */
  clickTransitDeclarationsMenuOption() {
    cy.xpath("//a[(normalize-space()='Transit Declarations')]").eq(0).invoke("show").click({force: true});
  }

  /**
   * clicks on Party Directory in menu
   */
  clickPartyDirectory() {
    cy.xpath("//div[@id='dtNavbar']//ul//a[normalize-space()='Party Directory']").invoke("show").click({ force: true });
  }

  /**
   * select location
   */
  clickSelectLocation() {
    cy.xpath(
      "//button[@data-id='portSelectionLocationPicker']//span[@class='filter-option pull-left'][normalize-space()='Select a Location...']"
    ).click({ force: true });
    cy.xpath("//ul[@aria-expanded='true']//a[@role='option'][normalize-space()='GB Border Locations']").click();
    cy.get("#portSelectorConfirm");
  }
}
export default Header;
