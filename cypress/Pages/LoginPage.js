/* eslint-disable no-undef */
/* eslint-disable class-methods-use-this */
class Login {
  enterUserName(strEmail) {
    cy.get("#username").type(strEmail);
  }

  enterPassword(strPassword) {
    cy.get("#password").type(strPassword);
  }

  clickLoginButton() {
    cy.get("#kc-login").click({ force: true });
  }
}
export default Login;
