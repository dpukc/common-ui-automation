/* eslint-disable no-undef */
/* eslint-disable class-methods-use-this */

/**
 * @author Zeeshan Asghar
 * Dated: 01/12/2021
 */

class MucrProcessing {
  // MucrProcessing page locators
  getMucrProcessingButton() {
    return cy.get("#mucr_btn");
  }

  getExportArrivalButton() {
    return cy.get("#exportArrival_btn");
  }

  getExportDeparturesButton() {
    return cy.get("#exportDeparture_btn");
  }

  getUCRTxt() {
    return cy.get("#ucr");
  }

  getLocationOfGoodsTxt() {
    return cy.get("#locationOfGoods");
  }

  getAddButton() {
    return cy.xpath("//button[@id='loc_label']");
  }

  getCspDropDown() {
    return cy.get("#csp_select");
  }

  getCspDropDownListItem(itemName) {
    return cy.xpath(`//span[normalize-space()='${itemName}']`);
  }

  getBadgeDropDown() {
    return cy.get("#badge_select");
  }

  getBadgeDropDownListItem(itemName) {
    return cy.xpath(`//mat-option/span[normalize-space()='${itemName}']`);
  }

  getArrivalButton() {
    return cy.get("#submit_btn");
  }

  getCloseConfirmationButton() {
    return cy.get("#close_btn");
  }

  /**
   * clicks on mucr processing button
   */
  clickMucrProcessingButton() {
    this.getMucrProcessingButton().click();
  }

  /**
   * clicks on export arrival button
   */
  clickExportArrivalButton() {
    this.getExportArrivalButton().click();
  }

  /**
   * type UCR Number
   */
  typeUCRNumber() {
    cy.readFile("cypress/data-files/dynamic-data/dynamic-data.json").then((obj) => {
      const dynamicData = obj;
      this.getUCRTxt().type(dynamicData.exportMucr);
    });
  }

  /**
   * type Location of good
   */
  typeLocatinOfGoods(locationOfGoods) {
    this.getLocationOfGoodsTxt().type(locationOfGoods);
  }

  /**
   * clicks on add button
   */
  clickAddButton() {
    this.getAddButton().click();
  }

  /**
   * select Csp
   */
  selectCsp(itemName) {
    this.getCspDropDown().click();
    this.getCspDropDownListItem(itemName).click();
  }

  /**
   * select badge
   */
  selectBadge(itemName) {
    this.getBadgeDropDown().click();
    this.getBadgeDropDownListItem(itemName).click();
  }

  /**
   * clicks on arrival button
   */
  clickArrivalButton() {
    this.getArrivalButton().click();
  }

  /**
   * clicks on close confirmation button
   */
  clickCloseConfirmationButton() {
    this.getCloseConfirmationButton().click();
    cy.wait(60000);
  }

  /**
   * clicks on export departure button
   */
  clickExportDepartureButton() {
    this.getExportDeparturesButton().click();
  }
}
export default MucrProcessing;
