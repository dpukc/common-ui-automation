/* eslint-disable no-undef */
/* eslint-disable class-methods-use-this */
/**
 * @author Ahmad Sohail
 * Dated: 01/12/2021
 */

 class PartyDirectory {

    constructor () {
        this.randomNum = "";
    }

    getPartyDropdown() {
        return cy.get("#addParty-selectpartyField #selectparty-select");
    }

    getPartyOption(party) {
        return cy.xpath(`//span[@class='mat-option-text'][normalize-space()='${party}']`);
    }

    getPartyName() {
        return cy.get("mat-form-field #addParty-partyNameCustomInput");
    }

    getShortCode() {
        return cy.get("mat-form-field #addParty-shortCodeCustomInput");
    }

    getState() {
        return cy.get("mat-form-field #addParty-stateCustomInput");
    }

    getPostcode() {
        return cy.get("mat-form-field #addParty-postCodeCustomInput");
    }

    getCountry() {
        return cy.get("mat-form-field #addParty-countryCustomInput");
    }

    getEORI() {
        return cy.get("mat-form-field #addParty-eoriCustomInput");
    }

    getSaveButton() {
        return cy.get("#save_btn");
    }

    getOKButton() {
        return cy.get("app-dialog button").eq(1);
    }

    getShortCodeOnSearch() {
        return cy.get("mat-form-field #search-short-code-customInput");
    }

    getSearchButton() {
        return cy.get("#search-btn");
    }

    getShortCodeFromList() {
        return cy.get("#shortCode-tdText");
    }

    /**
     * Select Party from multi select dropdown
    */
    selectParty(party) {
        this.getPartyDropdown().click();
        this.getPartyOption(party).click();
    }

    /**
     * Type Party Name 
    */
    typePartyName(partyName) {
        cy.get(".cdk-overlay-backdrop").eq(1).click();
        this.getPartyName().type(partyName);
    }

    /**
     * Type Short Code
    */
    typeShortCode() {
        cy.createRandomNumber().then(($value) => {
            this.randomNum = $value;
            this.getShortCode().type($value);
        });
    }

    /**
     * Type State Name
    */
    typeState(state) {
        this.getState().type(state);
    }
        
    /**
    * Type Postcode
    */
    typePostCode(postcode) {
        this.getPostcode().type(postcode);
    }
     
    /**
     * Select country from dropdown
    */
     selectCountry(country) {
        this.getCountry().type(country).type("{downarrow}").type("{enter}");
    }
    
    /**
     * Type EORI
    */
     typeEORI(eori) {
        this.getEORI().type(eori);
    } 
  
    clickSaveButton() {
        this.getSaveButton().click();
    }

    clickOKButton() {
        this.getOKButton().click();
    }

    /**
     * Type Short Code to filter search results
    */
    typeShortCodeForSearch() {
        this.getShortCodeOnSearch().type(this.randomNum);
    }

    /**
     * Click on Search Button
    */
    clickSearchButton() {
        this.getSearchButton().click();
    }

    /**
     * Verify Added Party appears on the list
    */
    verifyAddedParty() {
        this.getShortCodeFromList().should("have.text", this.randomNum);
    }
  }
  export default PartyDirectory;
  