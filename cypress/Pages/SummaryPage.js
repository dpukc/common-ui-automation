/* eslint-disable no-undef */
/* eslint-disable class-methods-use-this */

/**
 * @author Zeeshan Asghar
 * Dated: 03/11/2021
 */

class SummaryPage {
  // Locators
  getSubmitButton() {
    return cy.xpath("//span[normalize-space()='Submit']");
  }

  getAmendmendReason() {
    return cy.get("mat-dialog-content textarea");
  }

  getCsp() {
    return cy.get("mat-dialog-content textarea");
  }

  getSubmitButtonOfPopUp() {
    return cy.get("mat-dialog-actions button").eq(1);
  }

  /**
   * click submit button
   */
  clickSubmitButton() {
    this.getSubmitButton().click();
    cy.get("mat-dialog-container mat-form-field mat-select").click();
    cy.get("div.mat-select-panel mat-option").click();
    cy.get("mat-dialog-actions button").eq(1).click();
  }

  clickSubmitMultiBadge() {
    this.getSubmitButton().click();
    cy.get("mat-dialog-container mat-form-field mat-select").eq(0).click();
    cy.xpath("//mat-option/span[normalize-space()='CNS']").click();
    cy.get("mat-dialog-container mat-form-field mat-select").eq(1).click();
    cy.xpath("//mat-option/span[normalize-space()='Southampton']").click();
    cy.get("mat-dialog-actions button").eq(1).click();
  }

  /**
   * type Amendmend Reason
   */
  typeAmendmendReason() {
    this.getAmendmendReason().type("Automated Reason");
  }

  /**
   * type submit popup
   */
  clickSubmitOnPopUp() {
    this.getSubmitButtonOfPopUp().click();
  }
}
export default SummaryPage;
