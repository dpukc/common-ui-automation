/* eslint-disable no-undef */
/* eslint-disable class-methods-use-this */

/**
 * @author nidhi dave
 * Date : 13-05-2022
 */
/// <reference type = "">

class DeclarationDashboardPage {
  // Locators
  getTransitDashBoardUrl() {
    return cy.url();
  }

  getCreateDeclarationButton() {
    return cy.xpath("//span[normalize-space()='Create Declaration']");
  }

  getCopyToNewButton() {
    return cy.get("#declaration_btn_file_copy");
  }

  getActionButton() {
    return cy.xpath("//button[@id='declaration_btn_more']");
  }

  getSearchEntryReference() {
    return cy.xpath(
      "//span[.//mat-label[text() = ' Search LRN / MRN / Guarantee Reference / Vehicle Trailer ']]/ancestor::div/input"
    );
  }

  getSearchButton() {
    return cy.xpath("//button[@type = 'submit']");
  }

  getDeclarationEntryReference() {
    return cy.get("div.handCursor>span");
  }

  getDeclarationStatus() {
    return cy.get("td.mat-column-status>span");
  }

  transitDashboardIsDisplayed() {
    this.getTransitDashBoardUrl().should("include", "/transit-declaration");
  }

  getCancelButton() {
    return cy.xpath("//button[@id='declaration_btn_cancel']");
  }

  getCancelBtn() {
    return cy.xpath("(//span[contains(text(),'Cancel')])[1]");
  }

  getConfirmationPopupDeleteButton() {
    return cy.xpath("//button[@id='btn_isDelete_submit']");
  }

  getDeclarationDataTable() {
    return cy.xpath("//tfoot[@role='rowgroup']//tr[1]");
  }

  getShowMoreButton() {
    return cy.get("#declaration_btn_showMore");
  }

  getStatusTextBox() {
    return cy.xpath("//input[@id='declaration_declarationStatus-customInput']");
  }

  getStatusDropDownItem() {
    return cy.xpath("//span[@class='_tooltip-option'][normalize-space()='Declaration Accepted (MRN allocated)']");
  }

  getDecStatusFirstRow() {
    return cy.xpath("//tbody/tr[1]/td[9]");
  }

  getCancellationReason() {
    return cy.xpath("//textarea");
  }

  getCancelSubmitButton() {
    return cy.xpath("//button[@id='cancelDeclaration_btn_submit']");
  }

  // functions
  clickOnCreateDeclarationButtonForTransit() {
    this.getCreateDeclarationButton().click();
  }

  enterLRNNumberInSearchTextBox() {
    cy.readFile("cypress/data-files/dynamic-data/dynamic-data-transit.json").then((obj) => {
      const dynamicData = obj;
      this.getSearchEntryReference().clear();
      this.getSearchEntryReference().type(dynamicData.entryReference);
    });
  }

  clickActionButton() {
    this.getActionButton().click();
  }

  clickCopyToNewButton() {
    this.getCopyToNewButton().click();
  }

  enterLRNInSearchForCopyToNew() {
    this.getSearchEntryReference().clear();
    this.getSearchEntryReference().type(Cypress.env("copyToNewLRN"));
  }

  enterLRNInSearchForCopyToNewSimp() {
    this.getSearchEntryReference().clear();
    this.getSearchEntryReference().type(Cypress.env("copyToNewLRNSimp"));
  }

  clickOnSearchButton() {
    this.getSearchButton().click();
  }

  clickCancelButton() {
    this.getCancelButton().click();
  }

  clickCancelBtn() {
    this.getCancelBtn().click();
  }

  clickConfirmationDeleteButton() {
    this.getConfirmationPopupDeleteButton().click();
  }

  clickShowMoreButton() {
    this.getShowMoreButton().click();
  }

  selectDeclarationStatus(declarationStatus) {
    this.getStatusTextBox().click();
    cy.wait(5000);
    this.getStatusTextBox().type(declarationStatus);
    this.getStatusDropDownItem().click();
  }

  enterCancellationReason(cancellationReason) {
    this.getCancellationReason().type(cancellationReason);
  }

  clickCancelSubmitButton() {
    this.getCancelSubmitButton().click();
  }
}

export default DeclarationDashboardPage;
