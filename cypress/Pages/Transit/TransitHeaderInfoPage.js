/* eslint-disable no-undef */
/* eslint-disable class-methods-use-this */

class TransitHeaderInfoPage {
  // Get tomorrows date
  getTomorrowDate() {
    const currentDate = new Date();
    const tomorrowDate = new Date(currentDate);
    tomorrowDate.setDate(tomorrowDate.getDate() + 1);
    const newOutputDate = tomorrowDate.toLocaleDateString("en-UK");
    return newOutputDate;
  }

  // Locators
  //  **********************************************************************
  //              Get Element For Dispatch/Destination Details
  //  **********************************************************************
  getCountryOfDispatch() {
    return cy.xpath("//input[@id = 'dispatchCountry_customInput']");
  }

  getReferenceOfficeOfDeparture() {
    return cy.get("#departureCountry_input");
  }

  getCountryOfDestination() {
    return cy.xpath("//input[@id = 'destinationbCountry_customInput']");
  }

  getReferenceOfficeOfDestination() {
    return cy.get("#destinationCountry_input");
  }

  getCountryOfTermination() {
    return cy.xpath("//input[@id = 'terminationCountry_customInput']");
  }

  getReferenceOfficeOfTermination() {
    return cy.get("#terminationOffRefNo_input");
  }

  //  **********************************************************************
  //              Get Transit Details
  //  **********************************************************************
  getTransitOfficeAddButton() {
    return cy
      .xpath("//app-transit-details[@id='additionalInfo_customComponent']/div[1]/div/button/span")
      .scrollIntoView();
  }

  getTransitOfficeReferenceNumber() {
    return cy.get("#transitOffRefNo_input").scrollIntoView();
  }

  getTransitOfficeNumberFromDropDown() {
    return cy.xpath("//mat-option/span");
  }

  getExpectedTimeOfArrivalForTransitOffice() {
    return cy.get("#expected_Time_of_Arrival");
  }

  getTransitOfficeName() {
    return cy.get("#transitOffice_input");
  }

  getSaveButtonForAnyTable() {
    return cy.get("#save_btn");
  }

  //  **********************************************************************
  //              Get Element For Goods Details
  //  **********************************************************************

  getTotalNumberOfItemTexbox() {
    return cy
      .xpath("//span[.//mat-label[normalize-space() = 'Total Number of Items']]/ancestor::div/input[@type = 'text']")
      .scrollIntoView();
  }

  getTotalGrossMassTextBox() {
    return cy
      .xpath("//span[.//mat-label[normalize-space() = 'Total Gross Mass']]/ancestor::div/input[@type = 'text']")
      .scrollIntoView();
  }

  getTotalNumberOfPackagePieces() {
    return cy
      .xpath(
        "//span[.//mat-label[normalize-space() = 'Total Number of Packages / Pieces']]/ancestor::div/input[@type = 'text']"
      )
      .scrollIntoView();
  }

  //  **********************************************************************
  //              Get Seal Details
  //  **********************************************************************

  getSealDetailsAddButton() {
    return cy.xpath("//div[@id='sealInfo_addNew_col']//button[@id='add_btn']");
  }

  getSealIdentifier() {
    return cy.xpath("//input[@id='reference_customInput']");
  }

  getSealDetailsSaveButton() {
    return cy.xpath("//button[@id='save_btn']");
  }

  //  **********************************************************************
  //              Get Element For transport Details
  //  **********************************************************************
  getCustomSubPlace() {
    return cy
      .xpath("//span[.//mat-label[normalize-space() = 'Customs Sub Place']]/ancestor::div/input[@type = 'text']")
      .scrollIntoView();
  }

  getAuthorizedLocationOfGoods() {
    return cy.xpath("//input[@id='transportDetails_authLocGoodsCode_customInput']");
  }

  getPlaceOfLoading() {
    return cy.xpath("//input[@id='transportDetails_loadingLocationId_customInput']");
  }

  getIdentityMeansOfTransportAtDeparture() {
    return cy
      .xpath(
        "//span[.//mat-label[normalize-space() = 'Identity of Means of Transport at Departure (exp/trans)']]/ancestor::div/input[@type = 'text']"
      )
      .scrollIntoView();
  }

  getNationalityMeansOfTransportAtDeparture() {
    return cy
      .xpath(
        "//span[.//mat-label[normalize-space() = 'Nationality of Means of Transport at Departure']]/ancestor::div/input[@type = 'text']"
      )
      .scrollIntoView();
  }

  //  **********************************************************************
  //              Get Element For Guarantee Details
  //  **********************************************************************
  getAddButtonForGuarantee() {
    return cy.xpath("//mat-card[@id='guaranteeDetails_card']/div/button/span").scrollIntoView();
  }

  getGRNNumberField() {
    return cy.get("#guaranteeRefNo_input");
  }

  getEditGuaranteeDetailsButton() {
    return cy.xpath("//tr[@id='guaranteeDetails_row_loop']//div[@class='d-flex']//button[@id='edit_btn']");
  }

  getAccessCode() {
    return cy.get("#accessCode_input");
  }

  //  **********************************************************************
  //              Get Element For Cargo Containers Details
  //  **********************************************************************

  getCargocontainerizedChecbox() {
    return cy.get("#headerInfo_cargo-containerized-checkbox-input").scrollIntoView();
  }

  getAddButtonForCargoContainerOnHeaderInfoStepper() {
    return cy.xpath("//*[@id = 'transDetails_customComponent']//button[@id = 'add_btn']").scrollIntoView();
  }

  getCargoContainerNumberTextFiledOnHeaderInfoStepper() {
    return cy.xpath("//input[@id = 'type_customInput']").scrollIntoView();
  }

  //  **********************************************************************
  //              Get Element For Seal Details
  //  **********************************************************************
  getAddButtonForSealDetails() {
    return cy.xpath("//*[@id = 'sealInfo_formGroup']//button[@id = 'add_btn']").scrollIntoView();
  }

  getSealNumberTextFiled() {
    return cy.xpath("//input[@id = 'reference_customInput']").scrollIntoView();
  }

  getNextBtn() {
    return cy.xpath("//app-header-info[@id='create_header_customComponent']//button[@id='next_btn']");
  }

  //  **********************************************************************
  //              Enter Dispatch/Destination Details
  //  **********************************************************************
  enterCountryOfDispatch(countryOfDestination) {
    this.getCountryOfDispatch().type(countryOfDestination).type("{enter}");
  }

  enterReferenceOfficeOfDeparture(referenceOfficeOfDeparture) {
    this.getReferenceOfficeOfDeparture().type(referenceOfficeOfDeparture).type("{enter}");
  }

  enterCountryOfDestination(countryOfDestination) {
    this.getCountryOfDestination(countryOfDestination).type(countryOfDestination).type("{enter}");
  }

  enterReferenceOfficeOfDestination(referenceOfficeOfDestination) {
    this.getReferenceOfficeOfDestination().type(referenceOfficeOfDestination).type("{enter}");
  }

  enterCountryOfTermination(countryOfTermination) {
    this.getCountryOfTermination().clear();
    this.getCountryOfTermination().type(countryOfTermination).type("{enter}");
  }

  enterReferenceOfficeOfTermination(referenceOfficeOfTermination) {
    this.getReferenceOfficeOfTermination().clear();
    this.getReferenceOfficeOfTermination().type(referenceOfficeOfTermination).type("{enter}");
  }

  //  **********************************************************************
  //              Enter Transit office Details
  //  **********************************************************************
  clickOnTransitOfficeAddButton() {
    this.getTransitOfficeAddButton().click();
  }

  enterTransitOfiiceReferenceNumber(transitOfficeReferenceNo) {
    this.getTransitOfficeReferenceNumber().type(transitOfficeReferenceNo);
    // Need to select dropdown value since tab function is not useful in tansit office table
    const xpathForOptionInDropDown = `//mat-option[@id = 'transitOffRefNo_options']//span/span[normalize-space()='${transitOfficeReferenceNo}']`;
    cy.xpath(xpathForOptionInDropDown).click({ force: true });
  }

  enterExpectedTimeOfArrivalForTransitOffice() {
    this.getExpectedTimeOfArrivalForTransitOffice().clear();
    const expectedDate = this.getTomorrowDate().toString();
    this.getExpectedTimeOfArrivalForTransitOffice().type(expectedDate).tab();
  }

  clickOnTransitOfficeSaveButton() {
    this.getSaveButtonForAnyTable().click({ force: true });
  }

  //  **********************************************************************
  //              Enter Goods Details
  //  **********************************************************************

  enterTotalNumberOfItem(totalNumbeOfItem) {
    this.getTotalNumberOfItemTexbox().type(totalNumbeOfItem);
  }

  enterTotalGrossMass(totalGrossMass) {
    this.getTotalGrossMassTextBox().type(totalGrossMass);
  }

  enterTotalNumberOfPackagePieces(totalNumberOfPackagePieces) {
    this.getTotalNumberOfPackagePieces().type(totalNumberOfPackagePieces);
  }

  //  **********************************************************************
  //              Enter Seal Details
  //  **********************************************************************

  clickSealDetailsAddButton() {
    this.getSealDetailsAddButton().click();
  }

  enterEnterSealIdentifier(sealIdentifier) {
    this.getSealIdentifier().type(sealIdentifier);
  }

  clickSealDetailsSaveButton() {
    this.getSealDetailsSaveButton().click();
  }

  //  **********************************************************************
  //              Enter Goods location Details and transport details
  //  **********************************************************************
  enterCustomSubPlace(subPlace) {
    this.getCustomSubPlace().type(subPlace);
  }

  enterAuthorizedLocationOfGoods(location) {
    this.getAuthorizedLocationOfGoods().type(location);
  }

  enterPlaceOfLoading(place) {
    this.getPlaceOfLoading().type(place);
  }

  enterIdentityMeansOfTransportAtDeparture(identityMeansOfTransport) {
    this.getIdentityMeansOfTransportAtDeparture().type(identityMeansOfTransport);
  }

  enterNationalityMeansOfTransportAtDeparture(nationalityDeparture) {
    this.getNationalityMeansOfTransportAtDeparture().type(nationalityDeparture);
  }

  //  **********************************************************************
  //              Enter Guarantee details
  //  **********************************************************************
  clickOnGuaranteeDetailsAddButton() {
    this.getAddButtonForGuarantee().click();
  }

  clickEditGuaranteeDetails() {
    this.getEditGuaranteeDetailsButton().click();
  }

  enterGrnNumberInGuarantee(enterGRN) {
    this.getGRNNumberField().type(enterGRN);
  }

  enterAccessCodeForGrn(enterAccessCode) {
    this.getAccessCode().type(enterAccessCode);
  }

  clickOnsaveButtonForGuarantee() {
    this.getSaveButtonForAnyTable().click();
  }

  //  **********************************************************************
  //              Enter cargo details
  //  **********************************************************************
  checkCargoContainerizedCheckbox() {
    this.getCargocontainerizedChecbox().check({ force: true });
  }

  clickOnAddButtonForCargoContainerOnHeaderInfoStepper() {
    this.getAddButtonForCargoContainerOnHeaderInfoStepper().click();
  }

  enterCargoContainerNumberOnHeaderInfoStepper(cargoContainerNumber) {
    this.getCargoContainerNumberTextFiledOnHeaderInfoStepper().type(cargoContainerNumber);
  }

  //  **********************************************************************
  //              Enter Seal details
  //  **********************************************************************
  clickOnAddButtonForSealDetails() {
    this.getAddButtonForSealDetails().click();
  }

  enterSealNumberTextField(SealNumber) {
    this.getSealNumberTextFiled().type(SealNumber);
  }

  clickNextBtnHeaderInfo() {
    this.getNextBtn().click({ force: true });
  }
}
export default TransitHeaderInfoPage;
