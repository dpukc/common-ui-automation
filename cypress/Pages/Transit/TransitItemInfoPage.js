/* eslint-disable no-undef */
/* eslint-disable class-methods-use-this */

class TransitItemInfoPage {
  // Locators
  //  **********************************************************************
  //              Get Element For Item particular Details
  //  **********************************************************************
  getAddItemButton() {
    return cy.xpath("//span[normalize-space()= 'ADD NEW ITEM']");
  }

  getEditItemButton() {
    return cy.xpath("//tr[@id='dataTable_row_loop']//button[1]");
  }

  getEditSpecialMentionButton() {
    return cy.get("#additionalInfo_row_loop #edit_btn").eq(1);
  }

  getSpecialMentionConfirmationPopupButton() {
    return cy.xpath("//span[normalize-space()='Yes']");
  }

  getSpecialMentionSaveButton() {
    return cy.xpath("//div[@class='d-flex']//button[@id='save_btn']");
  }

  getCommodityCode() {
    return cy.get("#comodityCode_customInput").scrollIntoView();
  }

  getItemGrossMassTextBox() {
    return cy
      .xpath("//*[@id = 'itemInfo_itemParticular_component']//input[@id = 'item_particulars_gross_mass_value']")
      .scrollIntoView();
  }

  getItemNetMassTextBox() {
    return cy
      .xpath("//*[@id = 'itemInfo_itemParticular_component']//input[@id = 'item_particulars_net_mass_value']")
      .scrollIntoView();
  }

  getItemDescription() {
    return cy
      .xpath("//*[@id = 'itemInfo_itemParticular_component']//textarea[@id = 'item_particulars_description']")
      .scrollIntoView();
  }

  getItemValue() {
    return cy.xpath("//input[@id = 'item-value-customInput']").scrollIntoView();
  }

  //  **********************************************************************
  //              Get Element For Item->package section Details
  //  **********************************************************************

  getAddButtonForPackageSection() {
    return cy.xpath("//app-item-packages-details//span[text()= ' Add']").scrollIntoView();
  }

  getNumberOfPackageTextField() {
    return cy.xpath("//app-item-packages-details//app-custom-input//input").eq(0);
  }

  getNumberOfPiecesTextField() {
    return cy.xpath("//app-item-packages-details//app-custom-input//input").eq(1);
  }

  getKindOfPackageTextField() {
    return cy.xpath("//app-item-packages-details//app-custom-input//input").eq(2);
  }

  getMarkAndPackagesTextField() {
    return cy.xpath("//app-item-packages-details//app-custom-input//input").eq(3);
  }

  getSaveButtonForAnyTable() {
    return cy.xpath("//span[normalize-space()='save']");
  }

  getSaveItemInfoButton() {
    return cy.get("#save_btn");
  }

  //  **********************************************************************
  //              Get Element For Special Mentio Details
  //  **********************************************************************
  getAddButtonForSpecialMentionSection() {
    return cy.xpath("//app-item-special-mentions//span[text()= ' Add']").scrollIntoView();
  }

  getAdditionalInformationCode() {
    return cy.xpath("//tr[@id = 'additionalInfo_row_loop']//app-custom-input//input").eq(0);
  }

  getAdditionalInformationForSpecialMention() {
    return cy.xpath("//input[@id='specialMention_statementDescription-customInput']");
  }

  getCloseIconForAdditionalTypeCodeField() {
    return cy.xpath("//*[@id='specialMentions_statementCode-customInput']//mat-icon[normalize-space()='close']");
  }

  //  **********************************************************************
  //              Get Element For Previous Admin Dcoument Details
  //  **********************************************************************
  getAddButtonForPreviousAdminReference() {
    return cy.xpath("//div[@id='previousDocuments']//span");
  }

  getPreviousDocType() {
    return cy.xpath("//input[@id= 'priviousCertificate_typeCode-customInput']");
  }

  getPreviousDocumentCertificateReference() {
    return cy.xpath("//input[@id= 'priviousCertificate_typeCode_id-customInput']");
  }

  //  **********************************************************************
  //              Get Element For Cargo Container Details
  //  **********************************************************************
  getAddButtonForCargoContainerOnItemInfoStepper() {
    return cy.xpath("//*[@id = 'transportEquipments']//button[@id = 'add_btn']").scrollIntoView();
  }

  getCargoContainerNumberTextFiledOnItemInfoStepper() {
    return cy.xpath("//input[@id = 'cargoContainerNo_input']").scrollIntoView();
  }

  //  **********************************************************************
  //              Get Element For Produce Document Details
  //  **********************************************************************
  getAddButtonForProduceDocument() {
    return cy.xpath("//*[@id = 'producedDocuments']//button[@id = 'add_btn']").scrollIntoView();
  }

  getProduceDocumentCertificateTypeTextField() {
    return cy.xpath("//input[@id = 'producedDocument_docTypeCode-customInput']").scrollIntoView();
  }

  getProduceDocumentCertificateReferenceTextField() {
    return cy.xpath("//input[@id = 'producedDocument_docReference-customInput']").scrollIntoView();
  }

  //  **********************************************************************
  //              Get Element For Common Item Info buttons Details
  //  **********************************************************************

  getAddItemIcon() {
    return cy.get("[mattooltip='Add'] > #add_btn");
  }

  getSubmitButton() {
    return cy.xpath("//span[normalize-space() = 'Submit']");
  }
  //  **********************************************************************
  //              Add item Button and Enter Item particular details
  //  **********************************************************************

  clickOnAddItemButton() {
    this.getAddItemButton().click();
  }

  clickOnEditItemButton() {
    cy.wait(3000);
    this.getEditItemButton().click({ force: true });
  }

  enterCommodityCode(commodityCode) {
    this.getCommodityCode().type(commodityCode);
  }

  enterItemGrossMass(grossMassValue) {
    cy.wait(10);
    this.getItemGrossMassTextBox().type(grossMassValue);
  }

  enterItemNetMass(netMassValue) {
    this.getItemNetMassTextBox().type(netMassValue);
  }

  enterItemDescription(itemDescription) {
    this.getItemDescription().type(itemDescription);
  }

  enterItemValue(itemValue) {
    this.getItemValue().type(itemValue);
  }

  //  **********************************************************************
  //              Enter Package details
  //  **********************************************************************
  clickOnAddButtonForPackageSection() {
    this.getAddButtonForPackageSection().click();
  }

  enterKindOfPackage(kindOfPackage) {
    this.getKindOfPackageTextField().type(kindOfPackage);
  }

  enterNumberOfPackages(numberOfPackage) {
    this.getNumberOfPackageTextField().type(numberOfPackage);
  }

  enterNumberOfPeices(numberOfPeices) {
    this.getNumberOfPiecesTextField().type(numberOfPeices);
  }

  enterMarksAndNumberOfPackages(marksAndNumberOfPackages) {
    this.getMarkAndPackagesTextField().type(marksAndNumberOfPackages);
  }

  clickOnSaveButtonForAnyTable() {
    this.getSaveButtonForAnyTable().click();
  }

  clickSaveItemInfoButton() {
    this.getSaveItemInfoButton().click();
  }

  //  **********************************************************************
  //              Enter Special Mention details
  //  **********************************************************************
  clickOnAddButtonForSpecialMentionSection() {
    this.getAddButtonForSpecialMentionSection().click();
  }

  clickEditSpecialMentionSection() {
    this.getEditSpecialMentionButton().click();
  }

  clickSpecialMentionConfirmationPopup() {
    this.getSpecialMentionConfirmationPopupButton().click();
  }

  clickSpecialMentionSaveButton() {
    this.getSpecialMentionSaveButton().click();
  }

  enterAdditionalInformationCode(additionalInformationCode) {
    this.getAdditionalInformationCode().clear();
    this.getAdditionalInformationCode().type(additionalInformationCode);
    this.getAdditionalInformationCode().tab();
  }

  enterAdditionalInformation(additionalInformation) {
    this.getAdditionalInformationForSpecialMention().type(additionalInformation);
  }

  //  **********************************************************************
  //             Enter Previous Administration Document details
  //  **********************************************************************
  clickOnAddButtonForPreviousAdminReference() {
    this.getAddButtonForPreviousAdminReference().click();
  }

  enterPreviousDocType(previousDocumentType) {
    this.getPreviousDocType().type(previousDocumentType);
  }

  enterPreviousDocumentCertificateReference(previousDocumentTypeReference) {
    this.getPreviousDocumentCertificateReference().type(previousDocumentTypeReference);
  }

  //  **********************************************************************
  //              Enter cargo detail on item info stepper
  //  **********************************************************************
  clickOnAddButtonForCargoContainerOnItemInfoStepper() {
    this.getAddButtonForCargoContainerOnItemInfoStepper().click();
  }

  enterCargoContainerNumberOnItemInfoStepper(cargoContainerNumber) {
    this.getCargoContainerNumberTextFiledOnItemInfoStepper().type(cargoContainerNumber);
  }

  //  **********************************************************************
  //             Enter Produce Administration Document details
  //  **********************************************************************
  clickOnAddButtonForProduceDocument() {
    this.getAddButtonForProduceDocument().click();
  }

  enterProduceDocumentCertificateTypeTextField(previousDocumentType) {
    this.getProduceDocumentCertificateTypeTextField().type(previousDocumentType);
  }

  enterProduceDocumentCertificateReferenceTextField(previousDocumentTypeReference) {
    this.getProduceDocumentCertificateReferenceTextField().type(previousDocumentTypeReference);
  }

  //  **********************************************************************
  //            Common Item info Stepper Buttons
  //  **********************************************************************

  clickOnAddItemIcon() {
    this.getAddItemIcon().click();
  }

  clickOnSubmitButton() {
    this.getSubmitButton().click();
  }
}
export default TransitItemInfoPage;
