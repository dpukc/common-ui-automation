/* eslint-disable no-undef */
/* eslint-disable class-methods-use-this */

class TransitPartyInfoPage {
  getSimplifiedCheckbox() {
    return cy.get("label[for='isSimplified_declaration-input']");
  }

  // Enter Reference details
  getEntryReference() {
    return cy.get("#declarationDetails-declaration-refNo");
  }

  getDeclarationTypeTextBox() {
    // return cy.get("#declarationDetails-declaration-type");
    return cy.xpath("//input[@id='declarationType_label']");
  }

  getDeclarationPlaceTextbox() {
    return cy.get("#declarationDetails-ucr-refNo");
  }

  scrollToPrincipalSection() {
    cy.get("#partyInfo_principal_panel").scrollIntoView();
    window.scrollTo(cy.get("#mat-expansion-panel-header-3"));
  }

  getSaveAsDraftButton() {
    return cy.xpath("//mat-toolbar[@id='partyInfo_matFooter']//button[@id='saveDraft_btn']");
  }
  //  *******************************************************
  //              Get Element Principal Details
  //  *******************************************************

  getPrincipalName() {
    return cy.get("#principal_principal_field_col").scrollIntoView();
  }

  getPrincipalAdress1() {
    return cy.xpath("//input[@id = 'principleDetails_Street_and_Number_customInput1']");
  }

  getPrincipalAdress2() {
    return cy.xpath("//input[@id = 'principleDetails_Street_and_Number_customInput2']");
  }

  getPrincipalCity() {
    return cy.xpath("//input[@id = 'principleDetails_City_customInput']");
  }

  getPrincipalPostCode() {
    return cy.xpath("//input[@id = 'principal_postCode-customInput']");
  }

  getPrincipalCountry() {
    return cy.xpath("//input[@id = 'principal_country_customInput']");
  }

  getPrincipalEORI() {
    return cy.xpath("//input[@id = 'principleDetails_EORI_customInput']");
  }

  //  *******************************************************
  //              Get Element Consignor Details
  //  *******************************************************
  getConsignorName() {
    return cy.get("#consignor_input");
  }

  getConsignorStreetAddress1() {
    return cy.xpath("//input[@id= 'consignor_streetNo_customInput1']").scrollIntoView();
  }

  getConsignorStreetAddress2() {
    return cy.xpath("//input[@id= 'consignor_streetNo_customInput2']").scrollIntoView();
  }

  getConsignorCity() {
    return cy.xpath("//input[@id= 'consignor_city_customInput']");
  }

  getConsignorPostCode() {
    return cy.xpath("//input[@id= 'consignor_postalCode_customInput']");
  }

  getConsignorCountry() {
    return cy.xpath("//input[@id = 'consignor_country_customInput']");
  }

  getConsignorEORI() {
    return cy.xpath("//input[@id= 'consignor_eori_customInput']");
  }

  //  *******************************************************
  //              Get Element Consignee Details
  //  *******************************************************
  getConsigneeName() {
    return cy.get("#consignee_input");
  }

  getConsigneeStreetAddress1() {
    return cy.xpath("//input[@id = 'consigneeDetails_street_and_number_customInput1']").scrollIntoView();
  }

  getConsigneeStreetAddress2() {
    return cy.xpath("//input[@id = 'consigneeDetails_street_and_number_customInput2']").scrollIntoView();
  }

  getConsigneeCity() {
    return cy.xpath("//input[@id = 'consigneeDetails_city_customInput']");
  }

  getConsigneePostCode() {
    return cy.xpath("//input[@id= 'consignee_postCode-customInput']");
  }

  getConsigneeCountry() {
    return cy.xpath("//input[@id= 'consignee_country_customInput']");
  }

  getConsigneeEORI() {
    return cy.xpath("//input[@id = 'consigneeDetails_EORI_customInput']");
  }

  //  *******************************************************
  //              Get Element Next button
  //  *******************************************************
  getNextButton() {
    // return cy.get("#next_btn").scrollIntoView();
    return cy.xpath("//mat-toolbar[@id='partyInfo_matFooter']//button[@id='next_btn']").scrollIntoView();
  }

  clickSimplifiedCheckbox() {
    this.getSimplifiedCheckbox().click();
  }

  //  *******************************************************
  //              Enter Reference Details
  //  *******************************************************

  /**
   * Generates random entry refernce and type
   */
  typeEntryReferenceForTransit() {
    cy.createRandomAlphaNumeric().then(($value) => {
      this.getEntryReference().type($value);
      cy.writeDataInFile("cypress/data-files/dynamic-data/dynamic-data-transit.json", "entryReference", $value);
    });
  }

  selectDeclarationType(declarationType) {
    this.getDeclarationTypeTextBox().click();
    cy.wait(5000);
    this.getDeclarationTypeTextBox().type(declarationType);
    // cy.wait(2000);
    // Need to select dropdown value since tab function is not useful in tansit office table
    const xpathForOptionInDropDown = `//*[text()[contains(.,' ${declarationType} -')]]`;
    cy.xpath(xpathForOptionInDropDown).click({ force: true });
  }

  enterDeclarationPlace(enterDeclarationPlace) {
    this.getDeclarationPlaceTextbox().type(enterDeclarationPlace).type("{enter}");
  }

  //  *******************************************************
  //              Enter Principal Details
  //  *******************************************************

  enterPrincipalName(enterPrincipalName) {
    this.getPrincipalName().type(enterPrincipalName).type("{enter}");
  }

  enterPrincipalAdress1(enterAddress1) {
    this.getPrincipalAdress1().type(enterAddress1).type("{enter}");
  }

  enterPrincipalAdress2(enterAddress2) {
    this.getPrincipalAdress2().type(enterAddress2).type("{enter}");
  }

  enterPrincipalCity(enterCity) {
    this.getPrincipalCity().type(enterCity).type("{enter}");
  }

  enterPrincipalPostCode(enterPostCode) {
    this.getPrincipalPostCode().type(enterPostCode).type("{enter}");
  }

  enterPrincipalCountry(enterCountry) {
    this.getPrincipalCountry().type(enterCountry).type("{enter}");
  }

  enterPrincipalEORI(enterEORI) {
    this.getPrincipalEORI().type(enterEORI).type("{enter}");
  }

  //  *******************************************************
  //              Enter Consignor Details
  //  *******************************************************
  enterConsignorName(ConsignorName) {
    this.getConsignorName().type(ConsignorName).type("{enter}");
  }

  enterConsignorAdress1(enterAddress1) {
    this.getConsignorStreetAddress1().type(enterAddress1).type("{enter}");
  }

  enterConsignorAdress2(enterAddress2) {
    this.getConsignorStreetAddress2().type(enterAddress2).type("{enter}");
  }

  enterConsignorCity(enterCity) {
    this.getConsignorCity().type(enterCity).type("{enter}");
  }

  enterConsignorPostCode(enterPostCode) {
    this.getConsignorPostCode().type(enterPostCode).type("{enter}");
  }

  enterConsignorCountry(enterCountry) {
    this.getConsignorCountry().type(enterCountry).type("{enter}");
  }

  enterConsignorEORI(enterEORI) {
    this.getConsignorEORI().type(enterEORI).type("{enter}");
  }

  //  *******************************************************
  //              Enter Consignee Details
  //  *******************************************************
  enterConsigneeName(ConsigneeName) {
    this.getConsigneeName().type(ConsigneeName).type("{enter}");
  }

  enterConsigneeAdress1(enterAddress1) {
    this.getConsigneeStreetAddress1().type(enterAddress1).type("{enter}");
  }

  enterConsigneeAdress2(enterAddress2) {
    this.getConsigneeStreetAddress2().type(enterAddress2).type("{enter}");
  }

  enterConsigneeCity(enterCity) {
    this.getConsigneeCity().type(enterCity).type("{enter}");
  }

  enterConsigneePostCode(enterPostCode) {
    this.getConsigneePostCode().type(enterPostCode).type("{enter}");
  }

  enterConsigneeCountry(enterCountry) {
    this.getConsigneeCountry().type(enterCountry).type("{enter}");
  }

  enterConsigneeEORI(enterEORI) {
    this.getConsigneeEORI().type(enterEORI).type("{enter}");
  }

  clickOnNextButton() {
    this.getNextButton().click({ force: true });
  }

  clickSaveAsDraftButton() {
    this.getSaveAsDraftButton().click();
  }
}
export default TransitPartyInfoPage;
