/* eslint-disable no-undef */
/* eslint-disable class-methods-use-this */
/**
 * @author Ahmad Sohail
 * Dated: 01/12/2021
 */

 class UserReporting {

    getUserReportingButton() {
        return cy.get("#report_btn");
    }

    getReportName() {
        return cy.get("mat-form-field #reportType_customInput");
    }

    getRunReportButton() {
        return cy.get("#run_btn");
    }

    getCsvButton() {
        return cy.get("#download_csv");
    }

    getExcelButton() {
        return cy.get("#download_xlsx");
    }

    /**
    * clicks on User Reporting Button
    */
    clickUserReportingButton() {
        this.getUserReportingButton().click();
    }

    /**
    * select Report Name
    */
    selectReportName(reportName) {
        this.getReportName().type(reportName).type("{downarrow}").type("{enter}");
    }

    /**
    * click on Run Report button
    */
    clickRunReportButton() {
        this.getRunReportButton().click();
    }

    /**
    * click on CSV button
    */
    clickFileTypeButton(reportType) {
        if(reportType === "csv") {this.getCsvButton().click();}
        else {this.getExcelButton().click();}
        cy.wait(3000);
    }

    /**
    * verify file is downloaded
    */
    verifyFileDownload() {
        cy.task('readXlsx', { file: 'Import Entry Breakdown - 1638444513861 + .xlsx', sheet: "Sheet" }).then((rows) => {
            // expect(rows.length).to.equal(543);
            cy.log(rows.length);
        });   
    }
}
  export default UserReporting;
  