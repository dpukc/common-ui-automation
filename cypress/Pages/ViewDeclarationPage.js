/* eslint-disable no-undef */
/* eslint-disable class-methods-use-this */

/**
 * @author Zeeshan Asghar
 * Dated: 29/11/2021
 */

class ViewDeclaration {
  // view declration page locators
  getMessagesTab() {
    return cy.xpath("//div[contains(text(),'MESSAGES')]");
  }

  getReportsTab() {
    return cy.xpath("//div[contains(text(),'REPORTS')]");
  }

  getQuerySubmittedMessageType() {
    return cy.xpath("//tbody/tr[2]/td[1]/span[1]");
  }

  getQueryReceivedMessageType() {
    return cy.xpath("//tbody/tr[1]/td[1]/span[1]");
  }

  getGt1H2ReportViewButton() {
    return cy.xpath("//tbody/tr[1]/td[3]/span[1]/mat-icon[1]");
  }

  getQueryReceivedViewButton() {
    return cy.xpath("//tbody/tr[1]/td[4]/span[1]/mat-icon[1]");
  }

  getQuerySubmittedViewButton() {
    return cy.xpath("//tbody/tr[2]/td[4]/span[1]/mat-icon[1]");
  }

  getMessageParagraph() {
    return cy.xpath("//p");
  }

  getCloseButton() {
    return cy.xpath("//span[normalize-space()='Close']");
  }

  /**
   * type entry reference
   */
  typeEntryReference() {
    cy.readFile("cypress/data-files/dynamic-data/dynamic-data.json").then((obj) => {
      const dynamicData = obj;
      this.getSearchEntryReference().clear();
      this.getSearchEntryReference().type(dynamicData.entryReference);
    });
  }

  /**
   * clicks on messages tab on view declration page to view messages
   */
  clickMessagesTab() {
    this.getMessagesTab().click();
  }

  /**
   * clicks on Reports tab on view declration page to view Report
   */
  clickReportsTab() {
    this.getReportsTab().click();
  }

  /**
   * clicks on eye button to view message submitted
   */
  clickQuerySubmittedViewButton() {
    this.getQuerySubmittedViewButton().click();
  }

  /**
   * clicks on eye button to view message received
   */
  clickQueryReceivedViewButton() {
    this.getQueryReceivedViewButton().click();
  }

  /**
   * clicks on close button to close message popup
   */
  clickCloseButton() {
    this.getCloseButton().click();
  }

  /**
   * clicks on close button to close message popup
   */
  clickGt1H2ViewButton() {
    this.getGt1H2ReportViewButton().click();
  }
}
export default ViewDeclaration;
