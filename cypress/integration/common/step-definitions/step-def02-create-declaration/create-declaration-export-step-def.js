/* eslint-disable no-undef */
// eslint-disable-next-line import/no-extraneous-dependencies
import { When, And } from "cypress-cucumber-preprocessor/steps";
import ChiefDeclaration from "../../../../Pages/ChiefDeclarationPage";
import PartyInfo from "../../../../Pages/PartyInfoPage";
import HeaderInfo from "../../../../Pages/HeaderInfoPage";
import ItemInfo from "../../../../Pages/ItemInfoPage";
import SummaryPage from "../../../../Pages/SummaryPage";
import MucrProcessing from "../../../../Pages/MucrProcessingPage";

/// <reference types="Cypress" />
const chiefDeclartion = new ChiefDeclaration();
const partyInfo = new PartyInfo();
const headerInfo = new HeaderInfo();
const itemInfo = new ItemInfo();
const summary = new SummaryPage();
const mucrProcessing = new MucrProcessing();

//  *******************************************************
//              Function for C88E_D declaration
//  *******************************************************

// Enter reference details for C88E_D
function enterReferenceDetailsForC88ED(datatable) {
  partyInfo.typeEntryReference();
  datatable.hashes().forEach((element) => {
    partyInfo.selectEntryType(element.EntryType);
    partyInfo.selectAdditionalType(element.AdditionalType);
    partyInfo.selectRepType(element.RepType);
    partyInfo.typeGrossWeight(element.GrossWeight);
    partyInfo.typePackages(element.Packages);
  });
}

// Enter consignor details for C88E_D
function enterConsignorDetailsForC88ED(datatable) {
  datatable.hashes().forEach((element) => {
    partyInfo.typeConsignorName(element.Consignor);
    partyInfo.typeConsignorAddress1(element.Address1);
    partyInfo.typeConsignorAddress2(element.Address2);
    partyInfo.typeConsignorCity(element.City);
    partyInfo.typeConsignorState(element.State);
    partyInfo.typeConsignorPostCode(element.PostCode);
    partyInfo.selectConsignorCountry(element.Country);
    partyInfo.typeConsignorEori(element.Eori);
  });
}

// Enter consignee details for C88E_D
function enterConsigneeDetailsForC88ED(datatable) {
  datatable.hashes().forEach((element) => {
    partyInfo.typeConsigneeName(element.Consignee);
    partyInfo.typeConsigneeAddress1(element.Address1);
    partyInfo.typeConsigneeAddress2(element.Address2);
    partyInfo.typeConsigneeCity(element.City);
    partyInfo.typeConsigneeState(element.State);
    partyInfo.typeConsigneePostCode(element.PostCode);
    partyInfo.selectConsigneeCountry(element.Country);
  });
}

// Enter agent details for C88E_D
function enterAgentDetailsForC88ED(datatable) {
  datatable.hashes().forEach((element) => {
    partyInfo.typeAgentName(element.AgentName);
    partyInfo.typeAgentAddress1(element.Address1);
    partyInfo.typeAgentAddress2(element.Address2);
    partyInfo.typeAgentCity(element.City);
    partyInfo.typeAgentState(element.State);
    partyInfo.typeAgentPostCode(element.PostCode);
    partyInfo.selectAgentCountry(element.Country);
    partyInfo.typeAgentEori(element.Eori);
  });
}

// Enter ware ho details for C88E_D
function enterWarehouseDetailsForC88ED(datatable) {
  datatable.hashes().forEach((element) => {
    partyInfo.typeWarehouseId(element.WarehouseId);
  });
}

// Enter transport details for C88E_D
function enterTransportDetailsForC88ED(datatable) {
  datatable.hashes().forEach((element) => {
    cy.log(element.DispatchCountry);
    headerInfo.selectDispatchCountry(element.DispatchCountry);
    headerInfo.selectDestinationCountry(element.DestinationCountry);
    headerInfo.selectVessel(element.Vessel);
    headerInfo.selectFlag(element.Flag);
    headerInfo.selectLocation(element.Location);
    headerInfo.selectTOT(element.TOT);
  });
}

// Enter custom details for C88E_D
function enterCustomDetailsForC88ED() {
  headerInfo.typeExportDUCR();
}
// Enter item terrif details for C88E_D
function enterTerrifDetailsForC88ED(datatable) {
  datatable.hashes().forEach((element) => {
    itemInfo.typeTerrifCode(element.TerrifCode);
    itemInfo.typeTerrifDescription(element.TerrifDescription);
  });
}

// Enter item quantities details for C88E_D
function enterQauntitiesDetailsForC88ED(datatable) {
  datatable.hashes().forEach((element) => {
    // itemInfo.typeQuantitiesStatisticalValue(element.StatisticalValue);
    itemInfo.typeQuantitiesValue(element.Value);
    itemInfo.selectQuantitiesCurrency(element.Currency);
    itemInfo.typeGross(element.Gross);
    itemInfo.typeQty1(element.Qty1);
  });
}

// Enter item  details for C88E_D
function enterItemDetailsForC88ED(datatable) {
  datatable.hashes().forEach((element) => {
    itemInfo.typeCpc(element.Cpc);
    itemInfo.typeDetailCoo(element.Coo);
  });
}

// Enter previous documents information for C88E_D
function addPreviousDocumentsForC88ED(datatable) {
  datatable.hashes().forEach((element) => {
    itemInfo.clickAddPreviousDocumentButton();
    itemInfo.selectClass(element.Class);
    itemInfo.selectType(element.Type);
    itemInfo.selectReference(element.Reference);
    itemInfo.clickSaveButton();
  });
}

// Add Packages for C88E_D
function addPackagesForC88ED(datatable) {
  datatable.hashes().forEach((element) => {
    itemInfo.clickAddPackagesButton();
    itemInfo.typeCount(element.Count);
    itemInfo.selectPackageKind(element.PackageKind);
    itemInfo.typeMarks(element.Marks);
    itemInfo.clickSavePackagesButton();
  });
}

// Add Documents for C88E_D
function addDocumentsForC88ED(datatable) {
  datatable.hashes().forEach((element) => {
    itemInfo.clickAddDocumentsButton();
    itemInfo.selectDocumentCode(element.Code);
    itemInfo.selectStatus(element.Status);
    itemInfo.typeDocumentReference(element.Reference);
    itemInfo.clickSaveDocumentsButton();
  });
}

//  *******************************************************
//              Function for C21E_J declaration
//  *******************************************************

// Enter reference details for C21E_J
function enterReferenceDetailsForC21EJ(datatable) {
  partyInfo.typeEntryReference();
  datatable.hashes().forEach((element) => {
    partyInfo.selectEntryType(element.EntryType);
    partyInfo.selectAdditionalType(element.AdditionalType);
    partyInfo.selectRepType(element.RepType);
    partyInfo.typeExportMUCR();
    partyInfo.typePackages(element.Packages);
  });
}

// Enter consignor details for C21E_J
function enterConsignorDetailsForC21EJ(datatable) {
  datatable.hashes().forEach((element) => {
    partyInfo.typeConsignorName(element.Consignor);
    partyInfo.typeConsignorAddress1(element.Address1);
    partyInfo.typeConsignorAddress2(element.Address2);
    partyInfo.typeConsignorCity(element.City);
    partyInfo.typeConsignorPostCode(element.PostCode);
    partyInfo.selectConsignorCountry(element.Country);
    partyInfo.typeConsignorEori(element.Eori);
  });
}

// Enter consignee details for C21E_J
function enterConsigneeDetailsForC21EJ(datatable) {
  datatable.hashes().forEach((element) => {
    partyInfo.typeConsigneeName(element.Consignee);
    partyInfo.typeConsigneeAddress1(element.Address1);
    partyInfo.typeConsigneeAddress2(element.Address2);
    partyInfo.typeConsigneeCity(element.City);
    partyInfo.typeConsigneePostCode(element.PostCode);
    partyInfo.selectConsigneeCountry(element.Country);
    partyInfo.typeConsigneeEori(element.Eori);
  });
}

// Enter agent details for C21E_J
function enterAgentDetailsForC21EJ(datatable) {
  datatable.hashes().forEach((element) => {
    partyInfo.typeAgentName(element.AgentName);
    partyInfo.typeAgentAddress1(element.Address1);
    partyInfo.typeAgentAddress2(element.Address2);
    partyInfo.typeAgentCity(element.City);
    partyInfo.typeAgentPostCode(element.PostCode);
    partyInfo.selectAgentCountry(element.Country);
    partyInfo.typeAgentEori(element.Eori);
  });
}

// Enter transport details for C21E_J
function enterTransportDetailsForC21EJ(datatable) {
  datatable.hashes().forEach((element) => {
    cy.log(element.DispatchCountry);
    headerInfo.selectLocation(element.Location);
  });
}

// Enter custom details for C21E_J
function enterCustomDetailsForC21EJ() {
  headerInfo.typeExportDUCR();
}
// Enter item terrif details for C21E_J
function enterTerrifDetailsForC21EJ(datatable) {
  datatable.hashes().forEach((element) => {
    itemInfo.typeTerrifDescription(element.TerrifDescription);
  });
}

// Enter item  details for C21E_J
function enterItemDetailsForC21EJ(datatable) {
  datatable.hashes().forEach((element) => {
    itemInfo.typeCpc(element.Cpc);
  });
}

// Enter previous documents information for C21E_J
function addPreviousDocumentsForC21EJ(datatable) {
  datatable.hashes().forEach((element) => {
    itemInfo.clickAddPreviousDocumentButton();
    itemInfo.selectClass(element.Class);
    itemInfo.selectType(element.Type);
    itemInfo.selectReference(element.Reference);
    itemInfo.clickSaveButton();
  });
}

// Add Packages for C21E_J
function addPackagesForC21EJ(datatable) {
  datatable.hashes().forEach((element) => {
    itemInfo.clickAddPackagesButton();
    itemInfo.typeCount(element.Count);
    itemInfo.selectPackageKind(element.PackageKind);
    itemInfo.typeMarks(element.Marks);
    itemInfo.clickSavePackagesButton();
  });
}

// Add Documents for C21E_J
function addDocumentsForC21EJ(datatable) {
  datatable.hashes().forEach((element) => {
    itemInfo.clickAddDocumentsButton();
    itemInfo.selectDocumentCode(element.Code);
    itemInfo.selectStatus(element.Status);
    itemInfo.typeDocumentReference(element.Reference);
    itemInfo.clickSaveDocumentsButton();
  });
}

//  *******************************************************
//              Function for ESXD declaration
//  *******************************************************

// Enter reference details
function enterReferenceDetailsForESXD(datatable) {
  partyInfo.typeEntryReference();
  datatable.hashes().forEach((element) => {
    partyInfo.selectEntryType(element.EntryType);
    partyInfo.selectAdditionalType(element.AdditionalType);
    partyInfo.selectRepType(element.RepType);
    partyInfo.typeGrossWeight(element.GrossWeight);
    partyInfo.typePackages(element.Packages);
  });
}

// Enter consignor details
function enterConsignorDetailsForESXD(datatable) {
  datatable.hashes().forEach((element) => {
    partyInfo.typeConsignorName(element.Consignor);
    partyInfo.typeConsignorAddress1(element.Address1);
    partyInfo.typeConsignorAddress2(element.Address2);
    partyInfo.typeConsignorCity(element.City);
    partyInfo.typeConsignorState(element.State);
    partyInfo.typeConsignorPostCode(element.PostCode);
    partyInfo.selectConsignorCountry(element.Country);
  });
}

// Enter consignee details
function enterConsigneeDetailsForESXD(datatable) {
  datatable.hashes().forEach((element) => {
    partyInfo.typeConsigneeEori(element.Eori);
  });
}

// Enter agent details
function enterAgentDetailsForESXD(datatable) {
  datatable.hashes().forEach((element) => {
    partyInfo.typeAgentName(element.AgentName);
    partyInfo.typeAgentAddress1(element.Address1);
    partyInfo.typeAgentAddress2(element.Address2);
    partyInfo.typeAgentCity(element.City);
    partyInfo.typeAgentState(element.State);
    partyInfo.typeAgentPostCode(element.PostCode);
    partyInfo.selectAgentCountry(element.Country);
    partyInfo.typeAgentEori(element.Eori);
  });
}

// Enter transport details
function enterTransportDetailsForESXD(datatable) {
  datatable.hashes().forEach((element) => {
    cy.log(element.DispatchCountry);
    headerInfo.selectDispatchCountry(element.DispatchCountry);
    headerInfo.selectDestinationCountry(element.DestinationCountry);
    headerInfo.selectLocation(element.Location);
  });
}

// Enter custom details
function enterCustomDetailsForESXD() {
  headerInfo.typeExportDUCR();
}
// Enter item terrif details
function enterTerrifDetailsForESXD(datatable) {
  datatable.hashes().forEach((element) => {
    itemInfo.typeTerrifDescription(element.TerrifDescription);
  });
}

// Enter item quantities details
function enterQauntitiesDetailsForESXD(datatable) {
  datatable.hashes().forEach((element) => {
    itemInfo.typeGross(element.Gross);
  });
}

// Enter item  details
function enterItemDetailsForESXD(datatable) {
  datatable.hashes().forEach((element) => {
    itemInfo.typeCpc(element.Cpc);
  });
}

// Add Packages
function addPackagesForESXD(datatable) {
  datatable.hashes().forEach((element) => {
    itemInfo.clickAddPackagesButton();
    itemInfo.typeCount(element.Count);
    itemInfo.selectPackageKind(element.PackageKind);
    itemInfo.typeMarks(element.Marks);
    itemInfo.clickSavePackagesButton();
  });
}

// Add Documents
function addDocumentsForESXD(datatable) {
  datatable.hashes().forEach((element) => {
    itemInfo.clickAddDocumentsButton();
    itemInfo.selectDocumentCode(element.Code);
    itemInfo.selectStatus(element.Status);
    itemInfo.typeDocumentReference(element.Reference);
    itemInfo.clickSaveDocumentsButton();
  });
}

//  *******************************************************
//              Function for C21E_K declaration
//  *******************************************************

// Enter reference details
function enterReferenceDetailsForC21EK(datatable) {
  partyInfo.typeEntryReference();
  datatable.hashes().forEach((element) => {
    partyInfo.selectEntryType(element.EntryType);
    partyInfo.selectAdditionalType(element.AdditionalType);
    partyInfo.selectRepType(element.RepType);
    partyInfo.typePackages(element.Packages);
  });
}

// Enter consignor details
function enterConsignorDetailsForC21EK(datatable) {
  datatable.hashes().forEach((element) => {
    partyInfo.typeConsignorName(element.Consignor);
    partyInfo.typeConsignorAddress1(element.Address1);
    partyInfo.typeConsignorAddress2(element.Address2);
    partyInfo.typeConsignorCity(element.City);
    partyInfo.typeConsignorState(element.State);
    partyInfo.typeConsignorPostCode(element.PostCode);
    partyInfo.selectConsignorCountry(element.Country);
    partyInfo.typeConsignorEori(element.Eori);
  });
}

// Enter consignee details
function enterConsigneeDetailsForC21EK(datatable) {
  datatable.hashes().forEach((element) => {
    partyInfo.typeConsigneeName(element.Consignee);
    partyInfo.typeConsigneeAddress1(element.Address1);
    partyInfo.typeConsigneeAddress2(element.Address2);
    partyInfo.typeConsigneeCity(element.City);
    partyInfo.typeConsigneeState(element.State);
    partyInfo.typeConsigneePostCode(element.PostCode);
    partyInfo.selectConsigneeCountry(element.Country);
    partyInfo.typeConsigneeEori(element.Eori);
  });
}

// Enter agent details
function enterAgentDetailsForC21EK(datatable) {
  datatable.hashes().forEach((element) => {
    partyInfo.typeAgentName(element.AgentName);
    partyInfo.typeAgentAddress1(element.Address1);
    partyInfo.typeAgentAddress2(element.Address2);
    partyInfo.typeAgentCity(element.City);
    partyInfo.typeAgentState(element.State);
    partyInfo.typeAgentPostCode(element.PostCode);
    partyInfo.selectAgentCountry(element.Country);
    partyInfo.typeAgentEori(element.Eori);
  });
}

// Enter transport details
function enterTransportDetailsForC21EK(datatable) {
  datatable.hashes().forEach((element) => {
    headerInfo.selectLocation(element.Location);
  });
}

// Enter custom details
function enterCustomDetailsForC21EK() {
  headerInfo.typeExportDUCR();
}
// Enter item terrif details
function enterTerrifDetailsForC21EK(datatable) {
  datatable.hashes().forEach((element) => {
    itemInfo.typeTerrifDescription(element.TerrifDescription);
  });
}

// Enter item quantities details
function enterQauntitiesDetailsForC21EK(datatable) {
  datatable.hashes().forEach((element) => {
    itemInfo.typeGross(element.Gross);
  });
}

// Enter item  details
function enterItemDetailsForC21EK(datatable) {
  datatable.hashes().forEach((element) => {
    itemInfo.typeCpc(element.Cpc);
  });
}

// Enter previous documents information
function addPreviousDocumentsForC21EK(datatable) {
  datatable.hashes().forEach((element) => {
    itemInfo.clickAddPreviousDocumentButton();
    itemInfo.selectClass(element.Class);
    itemInfo.selectType(element.Type);
    itemInfo.selectReference(element.Reference);
    itemInfo.clickSaveButton();
  });
}

// Add Packages
function addPackagesForC21EK(datatable) {
  datatable.hashes().forEach((element) => {
    itemInfo.clickAddPackagesButton();
    itemInfo.typeCount(element.Count);
    itemInfo.selectPackageKind(element.PackageKind);
    itemInfo.typeMarks(element.Marks);
    itemInfo.clickSavePackagesButton();
  });
}

// Add Documents
function addDocumentsForC21EK(datatable) {
  datatable.hashes().forEach((element) => {
    itemInfo.clickAddDocumentsButton();
    itemInfo.selectDocumentCode(element.Code);
    itemInfo.selectStatus(element.Status);
    itemInfo.typeDocumentReference(element.Reference);
    itemInfo.clickSaveDocumentsButton();
  });
}

//  *******************************************************
//              Step definitions for C88E_D declaration
//  *******************************************************
When("I enter reference details for C88E_D", enterReferenceDetailsForC88ED);
When("I enter consignor details for C88E_D", enterConsignorDetailsForC88ED);
When("I enter Consignee details1 for C88E_D", enterConsigneeDetailsForC88ED);
When("I enter agent details for C88E_D", enterAgentDetailsForC88ED);
When("I enter warehouse details for C88E_D", enterWarehouseDetailsForC88ED);
And("I enter transport details for C88E_D", enterTransportDetailsForC88ED);
And("I enter custom details for C88E_D", enterCustomDetailsForC88ED);
When("I enter terrif details for C88E_D", enterTerrifDetailsForC88ED);
When("I enter quantities for C88E_D", enterQauntitiesDetailsForC88ED);
When("I enter item details for C88E_D", enterItemDetailsForC88ED);
When("I add previous documents for C88E_D", addPreviousDocumentsForC88ED);
When("I enter Packages for C88E_D", addPackagesForC88ED);
When("I enter documents for C88E_D", addDocumentsForC88ED);

//  *******************************************************
//              Step definitions for C21E_J declaration
//  *******************************************************
When("I enter reference details for C21E_J", enterReferenceDetailsForC21EJ);
When("I enter consignor details for C21E_J", enterConsignorDetailsForC21EJ);
When("I enter Consignee details1 for C21E_J", enterConsigneeDetailsForC21EJ);
When("I enter agent details for C21E_J", enterAgentDetailsForC21EJ);
And("I enter transport details for C21E_J", enterTransportDetailsForC21EJ);
And("I enter custom details for C21E_J", enterCustomDetailsForC21EJ);
When("I enter terrif details for C21E_J", enterTerrifDetailsForC21EJ);
When("I enter item details for C21E_J", enterItemDetailsForC21EJ);
When("I add previous documents for C21E_J", addPreviousDocumentsForC21EJ);
When("I enter Packages for C21E_J", addPackagesForC21EJ);
When("I enter documents for C21E_J", addDocumentsForC21EJ);
When("I click on mucr processing button", () => {
  mucrProcessing.clickMucrProcessingButton();
});
When("I click on export arrival button", () => {
  mucrProcessing.clickExportArrivalButton();
});
When("I enter Mucr and location", () => {
  mucrProcessing.typeUCRNumber();
  mucrProcessing.typeLocatinOfGoods("LHR");
});
When("I click on Add button to add export arrival", () => {
  mucrProcessing.clickAddButton();
});
When("I select CSP and Badge", () => {
  mucrProcessing.selectCsp("CNS");
  mucrProcessing.selectBadge("Southampton");
});
When("I click on arrival button", () => {
  mucrProcessing.clickArrivalButton();
});
When("I clik on close confirmation button", () => {
  mucrProcessing.clickCloseConfirmationButton();
});

When("I click on export departures button", () => {
  mucrProcessing.clickExportDepartureButton();
});

//  *******************************************************
//              Step definitions for ESXD declaration
//  *******************************************************

When("I enter reference details for EXS_D", enterReferenceDetailsForESXD);
When("I enter consignor details for EXS_D", enterConsignorDetailsForESXD);
When("I enter Consignee details1 for EXS_D", enterConsigneeDetailsForESXD);
When("I enter agent details for EXS_D", enterAgentDetailsForESXD);
When("I click next button", () => {
  partyInfo.clickNextButton();
});
And("I enter transport details for EXS_D", enterTransportDetailsForESXD);
And("I enter custom details for EXS_D", enterCustomDetailsForESXD);
And("I click next button to go to item info page", () => {
  headerInfo.clickNextButton();
});

When("I click add new item button", () => {
  itemInfo.clickAddItemInfoButton();
});
When("I enter terrif details for EXS_D", enterTerrifDetailsForESXD);
When("I enter quantities for EXS_D", enterQauntitiesDetailsForESXD);
When("I enter item details for EXS_D", enterItemDetailsForESXD);
When("I enter Packages for EXS_D", addPackagesForESXD);
When("I enter documents for EXS_D", addDocumentsForESXD);
When("I click Add button to add item", () => {
  itemInfo.clickAddButton();
});

When("I click next button to go to summary page", () => {
  itemInfo.clickNextButton();
});

When("I click on submit button to submit declaration", () => {
  // if (Cypress.env("environment") === "uat") {
  //   summary.clickSubmitButton();
  // } else {
  summary.clickSubmitMultiBadge();
  // }
});

// When("I click on submit button to submit declaration for C21E_J", () => {
//   summary.clickSubmitMultiBadge();
// });

Then("I click on create declaration button", () => {
  chiefDeclartion.clickCreateDeclaration();
});

//  *******************************************************
//              Step definitions for C21E_K declaration
//  *******************************************************
When("I enter reference details for C21E_K", enterReferenceDetailsForC21EK);
When("I enter consignor details for C21E_K", enterConsignorDetailsForC21EK);
When("I enter Consignee details1 for C21E_K", enterConsigneeDetailsForC21EK);
When("I enter agent details for C21E_K", enterAgentDetailsForC21EK);

And("I enter transport details for C21E_K", enterTransportDetailsForC21EK);
And("I enter custom details for C21E_K", enterCustomDetailsForC21EK);
When("I enter terrif details for C21E_K", enterTerrifDetailsForC21EK);
When("I enter quantities for C21E_K", enterQauntitiesDetailsForC21EK);
When("I enter item details for C21E_K", enterItemDetailsForC21EK);
When("I add previous documents for C21E_K", addPreviousDocumentsForC21EK);
When("I enter Packages for C21E_K", addPackagesForC21EK);
When("I enter documents for C21E_K", addDocumentsForC21EK);

// ************verify declaration status**************
And("I enter entry reference number in search entry on chief dashboard", () => {
  chiefDeclartion.typeEntryReference();
});
When("I click on Search button on chief dashboard", () => {
  chiefDeclartion.clickSearchButton();
});
When("Declaration status should be as {string}", (entryStatus) => {
  cy.readFile("cypress/data-files/dynamic-data/dynamic-data.json").then((obj) => {
    const dynamicData = obj;
    chiefDeclartion.getDeclarationEntryReference().should("have.text", dynamicData.entryReference);
  });
  chiefDeclartion.getDeclarationStatus().should("have.text", entryStatus);
});
