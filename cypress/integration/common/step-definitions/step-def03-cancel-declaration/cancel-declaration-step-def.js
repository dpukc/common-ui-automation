/* eslint-disable no-undef */
// eslint-disable-next-line import/no-extraneous-dependencies
import { Given, And } from "cypress-cucumber-preprocessor/steps";
import ChiefDeclaration from "../../../../Pages/ChiefDeclarationPage";

/// <reference types="Cypress" />

const chiefDeclartion = new ChiefDeclaration();

Given("I click on actions button", () => {
  chiefDeclartion.clickActionsButton();
});
And("I click on cancel button", () => {
  chiefDeclartion.clickCancelMenuItem();
});

And("I enter cancellation reason as {string}", (cancellationReason) => {
  chiefDeclartion.typeCancellationReason(cancellationReason);
});
And("I click submit button", () => {
  chiefDeclartion.clickSubmitButton();
});

And("I click cancel submit button", () => {
  chiefDeclartion.clickCancelSubmitButton();
});
And("I again click confirm submit button", () => {
  chiefDeclartion.clickCancelSubmitButton();
});
