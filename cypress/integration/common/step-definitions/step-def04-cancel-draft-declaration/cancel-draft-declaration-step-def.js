/* eslint-disable no-undef */
// eslint-disable-next-line import/no-extraneous-dependencies
import { Given, And } from "cypress-cucumber-preprocessor/steps";
import ChiefDeclaration from "../../../../Pages/ChiefDeclarationPage";
import PartyInfo from "../../../../Pages/PartyInfoPage";
/// <reference types="Cypress" />

const chiefDeclartion = new ChiefDeclaration();
const partyInfo = new PartyInfo();

Given("I enter reference details for draft cancellation", () => {
  partyInfo.typeEntryReference();
});

When("I click on save as draft button", () => {
  partyInfo.clickSaveAsDraftButton();
});

And("I click on cancel button to cancel draft", () => {
  chiefDeclartion.clickCancelDraftenuItem();
});

And("I click on delete button", () => {
  chiefDeclartion.clickDeleteButton();
});

Then("Draft declaration should deleted", () => {
  chiefDeclartion.getDeclrationDataTable().should("have.text", " No declarations found ");
});
