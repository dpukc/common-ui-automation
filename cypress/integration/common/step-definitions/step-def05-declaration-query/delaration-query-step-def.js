/* eslint-disable no-undef */
// eslint-disable-next-line import/no-extraneous-dependencies
import { Given, And } from "cypress-cucumber-preprocessor/steps";
import ChiefDeclaration from "../../../../Pages/ChiefDeclarationPage";
import ViewDeclaration from "../../../../Pages/ViewDeclarationPage";

/// <reference types="Cypress" />

const chiefDeclartion = new ChiefDeclaration();
const viewDeclaration = new ViewDeclaration();

Given("I click on actions button", () => {
  chiefDeclartion.clickActionsButton();
});
And("I click on query button", () => {
  chiefDeclartion.clickQueryMenuItem();
});

And("I select query type as {string}", (queryType) => {
  chiefDeclartion.clickSelectQueryButton();
  chiefDeclartion.selectQueryType(queryType);
});

And("I click on view", () => {
  chiefDeclartion.clickViewMenuItem();
});

And("I click on messages tab", () => {
  viewDeclaration.clickMessagesTab();
});

And("{string} query response should be received", (queryType) => {
  viewDeclaration.getQuerySubmittedMessageType().should("have.text", `${queryType} Query Submitted`);
  viewDeclaration.getQueryReceivedMessageType().should("have.text", `${queryType} Query Received`);
  viewDeclaration.clickQuerySubmittedViewButton();
  viewDeclaration.getMessageParagraph().should("not.be.empty");
  viewDeclaration.clickCloseButton();
  viewDeclaration.clickQueryReceivedViewButton();
  viewDeclaration.getMessageParagraph().should("not.be.empty");
  viewDeclaration.clickCloseButton();
});
