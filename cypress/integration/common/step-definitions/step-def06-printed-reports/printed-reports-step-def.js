/* eslint-disable no-undef */
// eslint-disable-next-line import/no-extraneous-dependencies
import { And, When } from "cypress-cucumber-preprocessor/steps";
import ViewDeclaration from "../../../../Pages/ViewDeclarationPage";

/// <reference types="Cypress" />

const viewDeclaration = new ViewDeclaration();

When("I click on reports tab", () => {
  viewDeclaration.clickReportsTab();
});

And("I click on dt1-h2 view report button to view report", () => {
  viewDeclaration.clickGt1H2ViewButton();
});

Then("The report should be open and contain data", () => {
  viewDeclaration.getMessageParagraph().should("not.be.empty");
  cy.get("#reports_dialog_title").should("not.be.empty");
  viewDeclaration.clickCloseButton();
});
