/* eslint-disable no-undef */
// eslint-disable-next-line import/no-extraneous-dependencies
import { When, And } from "cypress-cucumber-preprocessor/steps";
import Header from "../../../../Pages/HeaderPage";
import ChiefDeclaration from "../../../../Pages/ChiefDeclarationPage";
import PartyDirectory from "../../../../Pages/PartyDirectoryPage";
// import HeaderInfo from "../../../../Pages/HeaderInfoPage";
// import ItemInfo from "../../../../Pages/ItemInfoPage";
// import SummaryPage from "../../../../Pages/SummaryPage";

/// <reference types="Cypress" />

const header = new Header();
const chiefDeclartion = new ChiefDeclaration();
const partyDirectory = new PartyDirectory();
// const headerInfo = new HeaderInfo();
// const itemInfo = new ItemInfo();
// const summary = new SummaryPage();


//  *******************************************************
//              Function to Enter Party Details
//  *******************************************************

function enterPartyDetails(datatable) {
    datatable.hashes().forEach((element) => {
      if(element.SelectParty){partyDirectory.selectParty(element.SelectParty);}
      if(element.PartyName){partyDirectory.typePartyName(element.PartyName);}
      partyDirectory.typeShortCode();
      if(element.State){partyDirectory.typeState(element.State);}
      if(element.PostCode){partyDirectory.typePostCode(element.PostCode);}
      if(element.Eori){partyDirectory.selectCountry(element.Country);}
      if(element.Eori){partyDirectory.typeEORI(element.Eori);}
    });
}

And("I click on party directory link", () => {
    header.clickPartyDirectory();
});

And("Party directory  page opens up successfully", () => {
    cy.get(".text-primary.my-2").should("have.text", " Party Directory ");
});

And("I click on add party button", () => {
    chiefDeclartion.clickAddPartyButton();
});

When("I enter party details", enterPartyDetails);

And("I click on save button on add party screen", () => {
    partyDirectory.clickSaveButton();
});

And("I click on OK button on confirmation pop up", () => {
    partyDirectory.clickOKButton();
});

And("I enter short code to filter search results", () => {
    partyDirectory.typeShortCodeForSearch();
});

And("I click on Search button", () => {
    partyDirectory.clickSearchButton();
});

And("New party is created and appears on party list", () => {
    partyDirectory.verifyAddedParty();
});