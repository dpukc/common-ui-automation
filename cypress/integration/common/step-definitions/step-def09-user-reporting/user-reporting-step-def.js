/* eslint-disable no-undef */
// eslint-disable-next-line import/no-extraneous-dependencies
import { When, And } from "cypress-cucumber-preprocessor/steps";
import UserReporting from "../../../../Pages/UserReportingPage";

/// <reference types="Cypress" />

const userReporting = new UserReporting();


And("I click on user reporting button", () => {
    userReporting.clickUserReportingButton();
});

When("I enter report name {string}", (reportName) => {
    userReporting.selectReportName(reportName);
});

And("I click on run report button", () => {
    userReporting.clickRunReportButton();
});

And("I click on {string} button", (reportType) => {
    userReporting.clickFileTypeButton(reportType);
});

And("Report file should be downloaded", () => {
    userReporting.verifyFileDownload();
});