/* eslint-disable no-undef */
/* eslint-disable class-methods-use-this */

// eslint-disable-next-line import/no-extraneous-dependencies
import { When, And } from "cypress-cucumber-preprocessor/steps";
import Header from "../../../../../Pages/HeaderPage";
import DeclarationDashboardPage from "../../../../../Pages/Transit/DeclarationDashboardPage";
import TransitPartyInfo from "../../../../../Pages/Transit/TransitPartyInfoPage";
import TransitHeaderInfoPage from "../../../../../Pages/Transit/TransitHeaderInfoPage";
import TransitItemInfoPage from "../../../../../Pages/Transit/TransitItemInfoPage";

/// <reference types="Cypress" />

const header = new Header();
const declarationDashboardPage = new DeclarationDashboardPage();
const transitPartyInfoPage = new TransitPartyInfo();
const transitHeaderInfoPage = new TransitHeaderInfoPage();
const transitItemInfoPage = new TransitItemInfoPage();
//  *******************************************************
//              Function for T1 type declaration
//  *******************************************************

// Enter reference details for T1 type declartion
function enterReferenceDetailsForTransitDeclaration(datatable) {
  transitPartyInfoPage.typeEntryReferenceForTransit();
  datatable.hashes().forEach((element) => {
    transitPartyInfoPage.selectDeclarationType(element.DeclarationType);
    transitPartyInfoPage.enterDeclarationPlace(element.DeclarationPlace);
  });
}
/**
 *this function enters principal details for transit declaration
 * @param {Principalname,StreetNumber1,StreetNumber2,City,PostCode,Country,Eori} datatable
 */
function enterPrincipalDetailsForTransitDeclaration(datatable) {
  transitPartyInfoPage.scrollToPrincipalSection();
  datatable.hashes().forEach((element) => {
    transitPartyInfoPage.enterPrincipalName(element.Principalname);
    transitPartyInfoPage.enterPrincipalAdress1(element.StreetNumber1);
    transitPartyInfoPage.enterPrincipalAdress2(element.StreetNumber2);
    transitPartyInfoPage.enterPrincipalCity(element.City);
    transitPartyInfoPage.enterPrincipalPostCode(element.PostCode);
    transitPartyInfoPage.enterPrincipalCountry(element.Country);
    transitPartyInfoPage.enterPrincipalEORI(element.Eori);
  });
}
/**
 * This fucntion enter consignor details for transit declaration
 * @param {Consignorname,StreetNumber1,StreetNumber2,City,PostCode,Country,Eori} datatable
 */
function enterConsignorDetailsForTransitDeclaration(datatable) {
  datatable.hashes().forEach((element) => {
    transitPartyInfoPage.enterConsignorName(element.Consignorname);
    transitPartyInfoPage.enterConsignorAdress1(element.StreetNumber1);
    transitPartyInfoPage.enterConsignorAdress2(element.StreetNumber2);
    transitPartyInfoPage.enterConsignorCity(element.City);
    transitPartyInfoPage.enterConsignorPostCode(element.PostCode);
    transitPartyInfoPage.enterConsignorCountry(element.Country);
    transitPartyInfoPage.enterConsignorEORI(element.Eori);
  });
}

/**
 * This fucntion enter consignee details for transit declaration
 * @param {Consignorname,StreetNumber1,StreetNumber2,City,PostCode,Country,Eori} datatable
 */
function enterConsigneeDetailsForTransitDeclaration(datatable) {
  datatable.hashes().forEach((element) => {
    transitPartyInfoPage.enterConsigneeName(element.Consigneename);
    transitPartyInfoPage.enterConsigneeAdress1(element.StreetNumber1);
    transitPartyInfoPage.enterConsigneeAdress2(element.StreetNumber2);
    transitPartyInfoPage.enterConsigneeCity(element.City);
    transitPartyInfoPage.enterConsigneePostCode(element.PostCode);
    transitPartyInfoPage.enterConsigneeCountry(element.Country);
    transitPartyInfoPage.enterConsigneeEORI(element.Eori);
  });
}

/**
 * Enter dispatch, destination and termination details on header info stepper
 * @param {*} datatable
 */
function enterDispatchDestinationDetails(datatable) {
  datatable.hashes().forEach((element) => {
    transitHeaderInfoPage.enterCountryOfDispatch(element.Countryofdispatch);
    transitHeaderInfoPage.enterReferenceOfficeOfDeparture(element.ReferenceOfficeOfDeparture);
    transitHeaderInfoPage.enterCountryOfDestination(element.Countryofdestination);
    transitHeaderInfoPage.enterReferenceOfficeOfDestination(element.ReferenceOfficeOfDestination);
    transitHeaderInfoPage.enterCountryOfTermination(element.CountryofTermination);
    transitHeaderInfoPage.enterReferenceOfficeOfTermination(element.ReferenceOfficeOfTermination);
  });
}

/**
 * Enter transit office details on header info stepper
 * @param {*} datatable
 */
function enterTransitOfficeDetails(datatable) {
  transitHeaderInfoPage.clickOnTransitOfficeAddButton();
  datatable.hashes().forEach((element) => {
    transitHeaderInfoPage.enterTransitOfiiceReferenceNumber(element.TrasitOfficeReference);
  });
  transitHeaderInfoPage.enterExpectedTimeOfArrivalForTransitOffice();
  transitHeaderInfoPage.clickOnTransitOfficeSaveButton();
}

/**
 * Enter goods details on header info stepper
 * @param {*} datatable
 */
function enterGoodsDetails(datatable) {
  datatable.hashes().forEach((element) => {
    transitHeaderInfoPage.enterTotalNumberOfItem(element.TotalNumberOfItem);
    transitHeaderInfoPage.enterTotalGrossMass(element.TotalGrossMass);
    transitHeaderInfoPage.enterTotalNumberOfPackagePieces(element.TotalNumberOfPackagesPieces);
  });
}

/**
 * Enter transport details on header info stepper
 * @param {*} datatable
 */
function enterTransportDetails(datatable) {
  datatable.hashes().forEach((element) => {
    transitHeaderInfoPage.enterIdentityMeansOfTransportAtDeparture(element.IdentityMeansOfTransportAtDeparture);
    transitHeaderInfoPage.enterNationalityMeansOfTransportAtDeparture(element.NationalityMeansOfTransportAtDeparture);
  });
}

/**
 * enter goods location details on header info stepper
 * @param {*} datatable
 */
function enterGoodsLocationDetails(datatable) {
  datatable.hashes().forEach((element) => {
    transitHeaderInfoPage.enterCustomSubPlace(element.CustomSubPlace);
  });
}

/**
 * Enterguarantee details on header info stepper
 * @param {*} datatable
 */
function enterGuaranteeDetails(datatable) {
  transitHeaderInfoPage.clickOnGuaranteeDetailsAddButton();
  datatable.hashes().forEach((element) => {
    transitHeaderInfoPage.enterGrnNumberInGuarantee(element.GuaranteeReferenceNumber);
    transitHeaderInfoPage.enterAccessCodeForGrn(element.Accesscode);
  });
  transitHeaderInfoPage.clickOnsaveButtonForGuarantee();
}

/**
 * Enter item details on item info stepper
 * @param {Enter} datatable
 */
function enteritemParticularDetails(datatable) {
  datatable.hashes().forEach((element) => {
    transitItemInfoPage.enterCommodityCode(element.CommodityCode);
    transitItemInfoPage.enterItemGrossMass(element.GrossMass);
    transitItemInfoPage.enterItemNetMass(element.NetMass);
    transitItemInfoPage.enterItemDescription(element.ItemDescription);
    transitItemInfoPage.enterItemValue(element.ItemValue);
  });
}

/**
 * Enter package details on item info stepper
 * @param {*} datatable
 */
function enterPackageDetails(datatable) {
  transitItemInfoPage.clickOnAddButtonForPackageSection();
  datatable.hashes().forEach((element) => {
    transitItemInfoPage.enterKindOfPackage(element.KindOfPackage);
    transitItemInfoPage.enterNumberOfPackages(element.NumberOfPackage);
    transitItemInfoPage.enterMarksAndNumberOfPackages(element.MarksAndNumberOfPackages);
  });
  transitItemInfoPage.clickOnSaveButtonForAnyTable();
}

/**
 * Select "CAL" ad additional type code in special mention details on item info stepper
 * @param {*} datatable
 */
function enterCALSpecialMentionInformation(datatable) {
  transitItemInfoPage.clickOnAddButtonForSpecialMentionSection();
  datatable.hashes().forEach((element) => {
    transitItemInfoPage.enterAdditionalInformationCode(element.AdditionalInforamtionCode);
  });
  transitItemInfoPage.clickOnSaveButtonForAnyTable();
}

/**
 * Select anyother additional type code in special mention field, Item info stepper
 * @param {*} datatable
 */
function enterOtherSpecialMentionDetails(datatable) {
  transitItemInfoPage.clickOnAddButtonForSpecialMentionSection();
  datatable.hashes().forEach((element) => {
    transitItemInfoPage.enterAdditionalInformationCode(element.AdditionalInforamtionCode);
    transitItemInfoPage.enterAdditionalInformation(element.AdditionalInformation);
  });
  transitItemInfoPage.clickOnSaveButtonForAnyTable();
}

/**
 * Select previous administrator type document , Item info stepper
 * @param {*} datatable
 */
function enterpreviousadminreference(datatable) {
  transitItemInfoPage.clickOnAddButtonForPreviousAdminReference();
  datatable.hashes().forEach((element) => {
    transitItemInfoPage.enterPreviousDocType(element.PreviousDocumentCertificateType);
    transitItemInfoPage.enterPreviousDocumentCertificateReference(element.PreviousDocumentCertificateReference);
  });
  transitItemInfoPage.clickOnSaveButtonForAnyTable();
}

/**
 * Enter cargo details on header info stepper
 * @param {*} datatable
 */
function enterCaroDetailsOnHeaderInfoStepper(datatable) {
  transitHeaderInfoPage.checkCargoContainerizedCheckbox();
  transitHeaderInfoPage.clickOnAddButtonForCargoContainerOnHeaderInfoStepper();
  datatable.hashes().forEach((element) => {
    transitHeaderInfoPage.enterCargoContainerNumberOnHeaderInfoStepper(element.CargoContainerNumber);
  });
  transitItemInfoPage.clickOnSaveButtonForAnyTable();
}

/**
 * Enter cargo details on item info stepper
 * @param {*} datatable
 */
function enterCaroDetailsOnItemInfoStepper(datatable) {
  transitItemInfoPage.clickOnAddButtonForCargoContainerOnItemInfoStepper();
  datatable.hashes().forEach((element) => {
    transitItemInfoPage.enterCargoContainerNumberOnItemInfoStepper(element.CargoContainerNumber);
  });
  transitItemInfoPage.clickOnSaveButtonForAnyTable();
}

/**
 * Enter seal number details on header info stepper
 * @param {*} datatable
 */
function enterSealDetailsOnHeaderInfoStepper(datatable) {
  transitHeaderInfoPage.clickOnAddButtonForSealDetails();
  datatable.hashes().forEach((element) => {
    transitHeaderInfoPage.enterSealNumberTextField(element.SealNumber);
  });
  transitItemInfoPage.clickOnSaveButtonForAnyTable();
}

/**
 * Enter prodcue document and certificate details on item info stepper
 * @param {*} datatable
 */
function enterProduceDocumentCertificate(datatable) {
  transitItemInfoPage.clickOnAddButtonForProduceDocument();
  datatable.hashes().forEach((element) => {
    transitItemInfoPage.enterProduceDocumentCertificateTypeTextField(element.ProduceDocumentCertificateType);
    transitItemInfoPage.enterProduceDocumentCertificateReferenceTextField(element.ProduceDocumentCertificateReference);
  });
  transitItemInfoPage.clickOnSaveButtonForAnyTable();
}

/**
 * Enter number of pieces details on item info stepper
 * @param {*} datatable
 */
function enterPiecesDetailsInPackageSection(datatable) {
  transitItemInfoPage.clickOnAddButtonForPackageSection();
  datatable.hashes().forEach((element) => {
    transitItemInfoPage.enterKindOfPackage(element.KindOfPackage);
    transitItemInfoPage.enterNumberOfPeices(element.NumberOfPeices);
  });
  transitItemInfoPage.clickOnSaveButtonForAnyTable();
}

/**
 * Click on add item button icon
 */
function clickOnAddItemIcon() {
  transitItemInfoPage.clickOnAddItemIcon();
}
/**
 * this function click on next button and user navigates to Header info stepper
 */
function clickOnNextButton() {
  transitPartyInfoPage.clickOnNextButton();
  cy.wait(3000);
}

/**
 * click on transit declaration link from hamburger menu on main cns page
 */
function clickOnTransitDeclarationLink() {
  header.clickTransitDeclarationsMenuOption();
}

/**
 * verify that transit declaration page is displayed
 */
function transitDeclarationPageIsDisplayed() {
  declarationDashboardPage.transitDashboardIsDisplayed();
}

/**
 * click on Create declaration button on tranist declaration dashboard
 */
function clickOnCreateDeclarationButton() {
  declarationDashboardPage.clickOnCreateDeclarationButtonForTransit();
}

/**
 *
 */
function clickOnAddItemButton() {
  transitItemInfoPage.clickOnAddItemButton();
}

function clickOnSubmit() {
  transitItemInfoPage.clickOnSubmitButton();
}

function enterLRNNumberInSerch() {
  declarationDashboardPage.enterLRNNumberInSearchTextBox();
}

function clickOnSearchButton() {
  declarationDashboardPage.clickOnSearchButton();
  cy.wait(2000);
}

//  *******************************************************
//              Step definitions for transit declation
//  *******************************************************

And("I click on transit declarations link", clickOnTransitDeclarationLink);
And("transit declaration page opens up successfully", transitDeclarationPageIsDisplayed);
And("I click on create declaration button", clickOnCreateDeclarationButton);
When("I enter reference details for Transit declaration", enterReferenceDetailsForTransitDeclaration);
And("I enter principal details for Transit declaration", enterPrincipalDetailsForTransitDeclaration);
And("I enter consignor details for Transit declaration", enterConsignorDetailsForTransitDeclaration);
And("I enter consignee details for Transit declaration", enterConsigneeDetailsForTransitDeclaration);
And("I click on next button", clickOnNextButton);
And(
  "I enter dispatchdestination details on headerinfo stepper for Transit declaration",
  enterDispatchDestinationDetails
);
And("I enter transit office details for transit declaration", enterTransitOfficeDetails);
And("I enter goods total for transit declaration", enterGoodsDetails);
And("I enter goods location details for transit declaration", enterGoodsLocationDetails);
And("I enter transport details for transit declaration", enterTransportDetails);
And("I enter guarantee details for transit declaration", enterGuaranteeDetails);
And("I click on add item button for transit declaration", clickOnAddItemButton);
And("I enter item particular details for transit declaration", enteritemParticularDetails);
And("I enter package details for transit declaration", enterPackageDetails);
And("I enter CAL as special mention details for transit declaration", enterCALSpecialMentionInformation);
And("I enter previous administrative references for transit Declaration", enterpreviousadminreference);
And("I click on add item icon for transit declaration", clickOnAddItemIcon);
And("I click on submit button to submit transit declaration", clickOnSubmit);
And("I enter entry reference number in search entry on transit dashboard", enterLRNNumberInSerch);
And("I click on Search button on transit dashboard", clickOnSearchButton);
And("I enter cargo container at header info stepper for transit declaration", enterCaroDetailsOnHeaderInfoStepper);
And("I enter seal details for transit declaration", enterSealDetailsOnHeaderInfoStepper);
And("I enter cargo container at item info stepper for transit declaration", enterCaroDetailsOnItemInfoStepper);
And("I enter produce document certificate type for transit declaration", enterProduceDocumentCertificate);
And(
  "I enter peices details in package setion intem info stepper for transit declaration",
  enterPiecesDetailsInPackageSection
);
And(
  "I enter other additional code for special mention details for transit declaration",
  enterOtherSpecialMentionDetails
);
When("Declaration status should be as {string} for transit declaration", (entryStatus) => {
  cy.readFile("cypress/data-files/dynamic-data/dynamic-data-transit.json").then((obj) => {
    const dynamicData = obj;
    declarationDashboardPage.getDeclarationEntryReference().should("have.text", dynamicData.entryReference);
  });
  declarationDashboardPage.getDeclarationStatus().should("have.text", entryStatus);
});
