/* eslint-disable no-undef */
/* eslint-disable class-methods-use-this */

// eslint-disable-next-line import/no-extraneous-dependencies
import { And } from "cypress-cucumber-preprocessor/steps";

import TransitPartyInfo from "../../../../../Pages/Transit/TransitPartyInfoPage";
import TransitHeaderInfoPage from "../../../../../Pages/Transit/TransitHeaderInfoPage";

/// <reference types="Cypress" />

const transitPartyInfoPage = new TransitPartyInfo();
const transitHeaderInfoPage = new TransitHeaderInfoPage();

//  *******************************************************
//              Function for T1 type declaration
//  *******************************************************

/**
 * enter goods location details on header info stepper
 * @param {*} datatable
 */
function enterGoodsLocationDetails(datatable) {
  datatable.hashes().forEach((element) => {
    transitHeaderInfoPage.enterAuthorizedLocationOfGoods(element.AuthorizedLocationOfGoods);
    transitHeaderInfoPage.enterPlaceOfLoading(element.PlaceOfLoading);
  });
}

/**
 * enter seal details on header info stepper
 * @param {*} datatable
 */
function enterSealDetails(datatable) {
  transitHeaderInfoPage.clickSealDetailsAddButton();
  datatable.hashes().forEach((element) => {
    transitHeaderInfoPage.enterEnterSealIdentifier(element.SealIdentifier);
    transitHeaderInfoPage.clickSealDetailsSaveButton();
  });
}

//  *******************************************************
//              Step definitions for transit declation
//  *******************************************************

And("I click on simplified checkbox", () => {
  transitPartyInfoPage.clickSimplifiedCheckbox();
});

And("I enter seal details", enterSealDetails);
And("I enter goods location details for transit declaration for simp declaration", enterGoodsLocationDetails);
