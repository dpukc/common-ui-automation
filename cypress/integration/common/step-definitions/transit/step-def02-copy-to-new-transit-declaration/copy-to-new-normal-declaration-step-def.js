/* eslint-disable no-undef */
/* eslint-disable class-methods-use-this */

// eslint-disable-next-line import/no-extraneous-dependencies
import { And } from "cypress-cucumber-preprocessor/steps";

import DeclarationDashboardPage from "../../../../../Pages/Transit/DeclarationDashboardPage";
import TransitPartyInfo from "../../../../../Pages/Transit/TransitPartyInfoPage";
import TransitHeaderInfoPage from "../../../../../Pages/Transit/TransitHeaderInfoPage";
import TransitItemInfoPage from "../../../../../Pages/Transit/TransitItemInfoPage";

/// <reference types="Cypress" />

const declarationDashboardPage = new DeclarationDashboardPage();
const transitPartyInfoPage = new TransitPartyInfo();
const transitHeaderInfoPage = new TransitHeaderInfoPage();
const transitItemInfoPage = new TransitItemInfoPage();
//  *******************************************************
//              Function for T1 type declaration
//  *******************************************************

/**
 * Enter goods details on header info stepper
 * @param {*} datatable
 */
function enterGoodsDetails(datatable) {
  cy.wait(5000);
  datatable.hashes().forEach((element) => {
    transitHeaderInfoPage.enterTotalGrossMass(element.TotalGrossMass);
    transitHeaderInfoPage.enterTotalNumberOfPackagePieces(element.TotalNumberOfPackagesPieces);
  });
}

/**
 * Enter access code guarantee details
 * @param {*} datatable
 */
function enterAccessCode(datatable) {
  transitHeaderInfoPage.clickEditGuaranteeDetails();
  datatable.hashes().forEach((element) => {
    transitHeaderInfoPage.enterAccessCodeForGrn(element.Accesscode);
  });
  transitHeaderInfoPage.clickOnsaveButtonForGuarantee();

  cy.wait(3000);
}

/**
 * Enter item details on item info stepper
 * @param {Enter} datatable
 */
function enteritemParticularDetails(datatable) {
  datatable.hashes().forEach((element) => {
    transitItemInfoPage.enterCommodityCode(element.CommodityCode);
    transitItemInfoPage.enterItemGrossMass(element.GrossMass);
    transitItemInfoPage.enterItemNetMass(element.NetMass);
    transitItemInfoPage.enterItemDescription(element.ItemDescription);
    transitItemInfoPage.enterItemValue(element.ItemValue);

    transitItemInfoPage.clickEditSpecialMentionSection();
    transitItemInfoPage.clickSpecialMentionSaveButton();
    transitItemInfoPage.clickSpecialMentionConfirmationPopup();
    cy.wait(2000);
  });
}

//  *******************************************************
//              Step definitions for transit declaration
//  *******************************************************

And("I enter entry reference number in search entry on transit dashboard for copy to new", () => {
  declarationDashboardPage.enterLRNInSearchForCopyToNew();
});

And("I click action button", () => {
  declarationDashboardPage.clickActionButton();
});

And("I click copy to new button", () => {
  declarationDashboardPage.clickCopyToNewButton();
});

And("I enter reference details for Transit declaration for copy to new", () => {
  // cy.wait(10000);
  transitPartyInfoPage.typeEntryReferenceForTransit();
});

And("I enter goods total for transit declaration for copy to new", enterGoodsDetails);

And("I enter access code in guarantee details for transit declaration", enterAccessCode);

And("I click on next button on header info page", () => {
  transitHeaderInfoPage.clickNextBtnHeaderInfo();
});

And("I click on edit item button for transit declaration", () => {
  transitItemInfoPage.clickOnEditItemButton();
});

And("I click on save item icon for transit declaration", () => {
  transitItemInfoPage.clickSaveItemInfoButton();
});

And("I enter item particular details for transit declaration for copy to new", enteritemParticularDetails);
