/* eslint-disable no-undef */
/* eslint-disable class-methods-use-this */

// eslint-disable-next-line import/no-extraneous-dependencies
import { And } from "cypress-cucumber-preprocessor/steps";

import DeclarationDashboardPage from "../../../../../Pages/Transit/DeclarationDashboardPage";

/// <reference types="Cypress" />

const declarationDashboardPage = new DeclarationDashboardPage();

//  *******************************************************
//              Step definitions for transit declaration
//  *******************************************************

And("I enter entry reference number in search entry on transit dashboard for copy to new simplified", () => {
  declarationDashboardPage.enterLRNInSearchForCopyToNewSimp();
});
