/* eslint-disable no-undef */
/* eslint-disable class-methods-use-this */

// eslint-disable-next-line import/no-extraneous-dependencies
import { Then, And } from "cypress-cucumber-preprocessor/steps";
import DeclarationDashboardPage from "../../../../../Pages/Transit/DeclarationDashboardPage";
import TransitPartyInfo from "../../../../../Pages/Transit/TransitPartyInfoPage";

/// <reference types="Cypress" />

const declarationDashboardPage = new DeclarationDashboardPage();
const transitPartyInfoPage = new TransitPartyInfo();

//  **********************************************************
//              Step definitions for cancel draft declaration
//  **********************************************************
And("I click save as draft button", () => {
  transitPartyInfoPage.clickSaveAsDraftButton();
});

And("I click cancel button in dropdown", () => {
  declarationDashboardPage.clickCancelButton();
});

And("I click delete button on confirmation popup", () => {
  declarationDashboardPage.clickConfirmationDeleteButton();
});

Then("Transit draft declaration should be deleted successfully", () => {
  cy.wait(3000);
  declarationDashboardPage.getDeclarationDataTable().should("have.text", " No declarations found ");
});
