/* eslint-disable no-undef */
/* eslint-disable class-methods-use-this */

// eslint-disable-next-line import/no-extraneous-dependencies
import { Then, And } from "cypress-cucumber-preprocessor/steps";
import DeclarationDashboardPage from "../../../../../Pages/Transit/DeclarationDashboardPage";

/// <reference types="Cypress" />

const declarationDashboardPage = new DeclarationDashboardPage();

//  **********************************************************
//              Step definitions for cancel  declaration
//  **********************************************************
And("I click cancel button to cancel declaration", () => {
  declarationDashboardPage.clickCancelBtn();
});

And("I enter cancellation reason as {string} for transit declaration", (cancellationReason) => {
  declarationDashboardPage.enterCancellationReason(cancellationReason);
});

And("I click submit button to cancel transit declaration", () => {
  declarationDashboardPage.clickCancelSubmitButton();
});

Then("Declaration status should be {string}", () => {
  cy.wait(3000);
  declarationDashboardPage.getDecStatusFirstRow().should("have.text", "Cancellation Submitted");
});
