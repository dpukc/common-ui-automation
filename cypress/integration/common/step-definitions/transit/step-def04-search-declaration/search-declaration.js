/* eslint-disable no-undef */
/* eslint-disable class-methods-use-this */

// eslint-disable-next-line import/no-extraneous-dependencies
import { Then, And } from "cypress-cucumber-preprocessor/steps";
import DeclarationDashboardPage from "../../../../../Pages/Transit/DeclarationDashboardPage";

/// <reference types="Cypress" />

const declarationDashboardPage = new DeclarationDashboardPage();

//  **********************************************************
//              Step definitions for search declaration
//  **********************************************************
And("I click show more button", () => {
  declarationDashboardPage.clickShowMoreButton();
});
And("I select declaration status as {string}", (declarationStatus) => {
  declarationDashboardPage.selectDeclarationStatus(declarationStatus);
});

Then("Transit declartion should be search and have status {string}", (expectedStatus) => {
  declarationDashboardPage.getDecStatusFirstRow().should("have.text", expectedStatus);
});
