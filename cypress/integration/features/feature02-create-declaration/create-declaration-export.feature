Feature: Create Declaration Export

    Background: Login to the application
        Given I go to application
        When  I enter valid credentials
        And I click on login button
        Then Application home page opens successfully

    @create-declaration @export @smoke-front-end @C88E_D
    Scenario: Verify that user is able to create export declaration with for C88E_D
        Given I click on manage menu on the application header
        And   I click on chief declarations link
        And   Chief declaration page opens up successfully
        And   I click on create declaration button
        And  I enter reference details for C88E_D
            | EntryType | AdditionalType | RepType | Mucr | GrossWeight | Packages |
            | EX        | C88E_D         | 2       |      | 1234.5      | 10       |
        And I enter consignor details for C88E_D
            | Consignor        | Address1  | Address2    | City   | State | PostCode | Country | Eori           |
            | tv new consignor | 401 NORTH | LAKE STREET | LONDON | utah  | WC2N 5DU | GB      | GB174313477000 |
        And I enter Consignee details1 for C88E_D
            | Consignee | Address1 | Address2 | City | State | PostCode | Country | Eori |
            | CCCC      | A1A1     | A2A2A    | CITY | STATE | PCODE    | US      |      |
        And I enter agent details for C88E_D
            | AgentName | Address1 | Address2 | City   | State   | PostCode | Country | Eori           |
            | AGENT1    | C        | AGADRS2  | AGCITY | AGSTATE | AGPCODE  | GB      | GB615081466000 |
        And I enter warehouse details for C88E_D
            | WarehouseId |
            | E1923447GB  |
        And I click next button
        And I enter transport details for C88E_D
            | DispatchCountry | DestinationCountry | Vessel     | Flag | Location | TOT |
            | GB              | FR                 | A2B ENERGY | NU   | GBLGP    | 1   |
        And   I enter custom details for C88E_D
            | DUCRPartNo | DeferType1 | DeferNo1 | InvoiceAmount | Currency | VATAdjustment | CurrencyVAT |
            |            |            |          |               |          |               |             |
        And I click next button to go to item info page
        And I click add new item button
        And I enter terrif details for C88E_D
            | TerrifCode | TerrifDescription               |
            | 40169300   | ER OTHER THAN HARD RUBBER OTHER |
        And I enter quantities for C88E_D
            | Value | Currency        | StatisticalValue | Gross  | Qty1  | Qty2 |
            | 1000  | USD - US Dollar | 2,465.47         | 534.00 | 43.00 |      |
        And I enter item details for C88E_D
            | Adjustment | Percentage | Cpc     | Coo | Preference | ValMethod |
            |            |            | 3171000 | GB  |            |           |
        And I add previous documents for C88E_D
            | Class | Type | Reference |
            | Z     | 380  | 80790     |
        And I enter Packages for C88E_D
            | Count | PackageKind | Marks |
            | 45    | PK          | ADD/D |
        And I enter documents for C88E_D
            | Code | Status | Reference |
            | 9104 | XX     | GBOIE     |
        And I click Add button to add item
        And I click next button to go to summary page
        And I click on submit button to submit declaration
        And I enter entry reference number in search entry on chief dashboard
        When  I click on Search button on chief dashboard
        Then  Declaration status should be as 'Entry Accepted'

    @create-declaration @export @smoke-front-end @EXS_D
    Scenario: Verify that user is able to create export declaration with for EXS_D
        Given I click on manage menu on the application header
        And   I click on chief declarations link
        And   Chief declaration page opens up successfully
        And   I click on create declaration button
        When  I enter reference details for EXS_D
            | EntryType | AdditionalType | RepType | Mucr | GrossWeight | Packages |
            | EX        | EXS_D          | 2       |      | 1234.5      | 10       |
        And I enter consignor details for EXS_D
            | Consignor        | Address1  | Address2    | City   | State | PostCode | Country | Eori |
            | tv new consignor | 401 NORTH | LAKE STREET | LONDON | utah  | WC2N 5DU | US      |      |
        And I enter Consignee details1 for EXS_D
            | Consignee | Address1 | Address2 | City | State | PostCode | Country | Eori           |
            | CCCC      | A1A1     | A2A2A    | CITY | STATE | PCODE    | JE      | GB445809527006 |
        And I enter agent details for EXS_D
            | AgentName | Address1 | Address2 | City   | State   | PostCode | Country | Eori           |
            | AGENT1    | C        | AGADRS2  | AGCITY | AGSTATE | AGPCODE  | US      | GB615081466000 |
        And I click next button
        And I enter transport details for EXS_D
            | DispatchCountry | DestinationCountry | Vessel                     | Flag | Location | TOT |
            | GB              | NL                 | 2115RETESTING BY MASTHANBS | PA   | GBDEU    | 1   |
        And   I enter custom details for EXS_D
            | DUCRPartNo | DeferType1 | DeferNo1 | InvoiceAmount | Currency | VATAdjustment | CurrencyVAT |
            | D          | B          | 8104523  | 11,111.10     | GBP      | 222.20        | GBP         |
        And I click next button to go to item info page
        And I click add new item button
        And I enter terrif details for EXS_D
            | TerrifCode | TerrifDescription |
            | 6109100010 | T-shirt           |
        And I enter quantities for EXS_D
            | Value     | StatisticalValue | Gross | Qty1   | Qty2 |
            | 11,111.10 | 3                | 54    | 333.30 |      |
        And I enter item details for EXS_D
            | Adjustment | Percentage | Cpc     | Coo | Preference | ValMethod |
            | B          | 0          | 1000046 | CN  | 100        | 1         |
        And I enter Packages for EXS_D
            | Count | PackageKind | Marks |
            | 10    | PL          | ADDR  |
        And I enter documents for EXS_D
            | Code | Status | Reference |
            | N740 | AF     | 3232      |
        And I click Add button to add item
        And I click next button to go to summary page
        And I click on submit button to submit declaration
        And I enter entry reference number in search entry on chief dashboard
        When  I click on Search button on chief dashboard
        Then  Declaration status should be as 'Entry Accepted'

    @cancel-declaration @export @smoke-front-end @EXS
    Scenario: Verify that user is able to cancel export declaration for EXS
        Given I click on manage menu on the application header
        And   I click on chief declarations link
        And   Chief declaration page opens up successfully
        And   I enter entry reference number in search entry on chief dashboard
        When  I click on Search button on chief dashboard
        And   I click on actions button
        And   I click on cancel button
        And   I enter cancellation reason as 'cancellation reason'
        And   I click cancel submit button
        And   I again click confirm submit button
        Then  Declaration status should be as 'Cancellation Accepted'

    @create-declaration @export @smoke-front-end @C21E_K
    Scenario: Verify that user is able to create export declaration with for C21E_K
        Given I click on manage menu on the application header
        And   I click on chief declarations link
        And   Chief declaration page opens up successfully
        And   I click on create declaration button
        When  I enter reference details for C21E_K
            | EntryType | AdditionalType | RepType | Mucr | GrossWeight | Packages |
            | EX        | C21E_K         | 2       |      |             | 10       |
        And I enter consignor details for C21E_K
            | Consignor        | Address1  | Address2    | City   | State | PostCode | Country | Eori           |
            | tv new consignor | 401 NORTH | LAKE STREET | LONDON | utah  | WC2N 5DU | GB      | GB174313477000 |
        And I enter Consignee details1 for C21E_K
            | Consignee | Address1 | Address2 | City | State | PostCode | Country | Eori           |
            | CCCC      | A1A1     | A2A2A    | CITY | STATE | PCODE    | CN      | GB174313477000 |
        And I enter agent details for C21E_K
            | AgentName | Address1 | Address2 | City   | State   | PostCode | Country | Eori           |
            | AGENT1    | C        | AGADRS2  | AGCITY | AGSTATE | AGPCODE  | GB      | GB615081466000 |
        And I click next button
        And I enter transport details for C21E_K
            | DispatchCountry | DestinationCountry | Vessel | Flag | Location | TOT |
            |                 |                    |        |      | GBLON    |     |
        And   I enter custom details for C21E_K
            | DUCRPartNo | DeferType1 | DeferNo1 | InvoiceAmount | Currency | VATAdjustment | CurrencyVAT |
            | D          | B          | 8104523  | 11,111.10     | GBP      | 222.20        | GBP         |
        And I click next button to go to item info page
        And I click add new item button
        And I enter terrif details for C21E_K
            | TerrifCode | TerrifDescription |
            | 6109100010 | T-shirt           |
        And I enter item details for C21E_K
            | Adjustment | Percentage | Cpc     | Coo | Preference | ValMethod |
            |            |            | 1000041 |     |            |           |
        And I add previous documents for C21E_K
            | Class | Type | Reference |
            | Z     | 380  | 80790     |
        And I enter Packages for C21E_K
            | Count | PackageKind | Marks |
            | 10    | PK          | ADD/D |
        And I enter documents for C21E_K
            | Code | Status | Reference |
            | N825 | JE     | 34242     |
        And I click Add button to add item
        And I click next button to go to summary page
        And I click on submit button to submit declaration
        And I enter entry reference number in search entry on chief dashboard
        When  I click on Search button on chief dashboard
        Then  Declaration status should be as 'Entry Accepted'

# @create-declaration @export @smoke-front-end @C21E_J
# Scenario: Verify that user is able to create export declaration with for C21E_J
#     Given I click on manage menu on the application header
#     And   I click on chief declarations link
#     And   Chief declaration page opens up successfully
#     And   I click on create declaration button
#     And  I enter reference details for C21E_J
#         | EntryType | AdditionalType | RepType | Mucr | GrossWeight | Packages |
#         | EX        | C21E_J         | 2       |      |             | 10       |
#     And I enter consignor details for C21E_J
#         | Consignor        | Address1  | Address2    | City   | State | PostCode | Country | Eori           |
#         | tv new consignor | 401 NORTH | LAKE STREET | LONDON |       | WC2N 5DU | GB      | GB174313477000 |
#     And I enter Consignee details1 for C21E_J
#         | Consignee | Address1 | Address2 | City | State | PostCode | Country | Eori           |
#         | CCCC      | A1A1     | A2A2A    | CITY |       | PCODE    | US      | GB445809527006 |
#     And I enter agent details for C21E_J
#         | AgentName | Address1 | Address2 | City   | State | PostCode | Country | Eori           |
#         | AGENT1    | C        | AGADRS2  | AGCITY |       | AGPCODE  | GB      | GB615081466000 |
#     And I click next button
#     And I enter transport details for C21E_J
#         | DispatchCountry | DestinationCountry | Vessel | Flag | Location | TOT |
#         |                 |                    |        |      | GBLON    |     |
#     And   I enter custom details for C21E_J
#         | DUCRPartNo | DeferType1 | DeferNo1 | InvoiceAmount | Currency | VATAdjustment | CurrencyVAT |
#         |            |            |          |               |          |               |             |
#     And I click next button to go to item info page
#     And I click add new item button
#     And I enter terrif details for C21E_J
#         | TerrifCode | TerrifDescription               |
#         |            | ER OTHER THAN HARD RUBBER OTHER |
#     And I enter item details for C21E_J
#         | Adjustment | Percentage | Cpc     | Coo | Preference | ValMethod |
#         |            |            | 1000041 | GB  |            |           |
#     And I add previous documents for C21E_J
#         | Class | Type | Reference |
#         | Z     | 380  | 80790     |
#     And I enter Packages for C21E_J
#         | Count | PackageKind | Marks |
#         | 10    | PK          | ADD/D |
#     And I enter documents for C21E_J
#         | Code | Status | Reference |
#         | N825 | JE     | 34242     |
#     And I click Add button to add item
#     And I click next button to go to summary page
#     And I click on submit button to submit declaration
#     And I enter entry reference number in search entry on chief dashboard
#     And  I click on Search button on chief dashboard
#     And  Declaration status should be as 'Entry Accepted'
#     When I click on mucr processing button
#     And I click on export arrival button
#     And I enter Mucr and location
#     And I click on Add button to add export arrival
#     And I select CSP and Badge
#     And I click on arrival button
#     And I clik on close confirmation button
#     And I click on export arrival button
#     And I enter Mucr and location
#     And I click on Add button to add export arrival
#     And I select CSP and Badge
#     And I click on arrival button
#     And I clik on close confirmation button
#     And I click on manage menu on the application header
#     And I click on chief declarations link
#     And Chief declaration page opens up successfully
#     And I enter entry reference number in search entry on chief dashboard
#     And I click on Search button on chief dashboard
#     And Declaration status should be as 'Entry Arrived'
#     When I click on mucr processing button
#     And I click on export departures button
#     And I enter Mucr and location
#     And I click on Add button to add export arrival
#     And I select CSP and Badge
#     And I click on arrival button
#     And I clik on close confirmation button
#     And I click on chief declarations link
#     And Chief declaration page opens up successfully
#     And I enter entry reference number in search entry on chief dashboard
#     And I click on Search button on chief dashboard
#     And Declaration status should be as 'Entry Departed'
