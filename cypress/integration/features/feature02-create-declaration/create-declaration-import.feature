Feature: Create Declaration

    Background: Login to the application
        Given I go to application
        When  I enter valid credentials
            | username      | password   |
            | customukadmin | Admin@1234 |
        And I click on login button
        Then Application home page opens successfully

    @create-declaration @import @smoke-front-end @C88_D
    Scenario: Verify that user is able to create declaration successfully for c88_d
        Given I click on manage menu on the application header
        And   I click on chief declarations link
        And   Chief declaration page opens up successfully
        And   I click on create declaration button
        When  I enter reference details
            | EntryType | AdditionalType | RepType | InventoryReference | GrossWeight | Packages |
            | IM        | C88_D          | 2       |                    | 1234.5      | 10       |
        And I enter consignor details
            | Consignor        | Address1  | Address2    | City   | State | PostCode | Country | Eori |
            | tv new consignor | 401 NORTH | LAKE STREET | LONDON | utah  | WC2N 5DU | US      |      |
        And I enter Consignee details1
            | Consignee | Address1 | Address2 | City | State | PostCode | Country | Eori           |
            | CCCC      | A1A1     | A2A2A    | CITY | STATE | PCODE    | GB      | GB445809527006 |
        And I enter agent details
            | AgentName | Address1 | Address2 | City   | State   | PostCode | Country | Eori           |
            | AGENT1    | C        | AGADRS2  | AGCITY | AGSTATE | AGPCODE  | US      | GB417141383000 |
        And I click next button
        And I enter transport details
            | CWC | Vessel                     | Flag | Location | TOT |
            | CN  | 2115RETESTING BY MASTHANBS | PA   | GBSOU    | 1   |
        And   I enter custom details
            | DUCRPartNo | DeferType1 | DeferNo1 | InvoiceAmount | Currency | VATAdjustment | CurrencyVAT |
            | T          | B          | 8104523  | 11,111.10     | GBP      | 222.20        | GBP         |
        And I click next button to go to item info page
        And I click add new item button
        And I enter terrif details
            | TerrifCode | TerrifDescription |
            | 6109100010 | T-shirt           |
        And I enter quantities
            | Value     | StatisticalValue | Qty1   | Qty2   |
            | 11,111.10 | 3                | 222.20 | 333.30 |
        And I enter item details
            | Adjustment | Percentage | Cpc     | Coo | Preference | ValMethod |
            | B          | 0          | 4000000 | CN  | 100        | 1         |
        And I enter additional Information
            | Code  | StatementText |
            | LIC99 |               |
        And I add previous documents
            | Class | Type | Reference |
            | Z     | 380  | test      |
        And I enter Packages
            | Count | PackageKind | Marks |
            | 10    | CT          | test  |
        And I enter documents
            | Code | Status |
            | N934 | AE     |
        And I enter Tax Lines
            | Code | Trid | Mop |
            | A00  | F    | F   |
            | B00  | S    | G   |
        And I enter supervising office details
            | SupervisingOffice | Address1 | Address2 | City | State   | PostCode | Country |
            | AGENT1            | AA1      | AA2      | CITY | AGSTATE | AGPCODE  | US      |
        And I click Add button to add item
        And I click next button to go to summary page
        And I click on submit button to submit declaration
        And I enter entry reference number in search entry on chief dashboard
        When  I click on Search button on chief dashboard
        Then  Declaration status should be as 'Entry Accepted'


    @cancel-declaration @import @smoke-front-end @C88_D
    Scenario: Verify that user is able to cancel export declaration for C88_D
        Given I click on manage menu on the application header
        And   I click on chief declarations link
        And   Chief declaration page opens up successfully
        And   I enter entry reference number in search entry on chief dashboard
        When  I click on Search button on chief dashboard
        And   I click on actions button
        And   I click on cancel button
        And   I enter cancellation reason as 'cancellation reason'
        And   I click cancel submit button
        And   I again click confirm submit button
        Then  Declaration status should be as 'Cancellation Accepted'

    # Need to fix
    # @create-declaration @import @smoke-front-end @c21
    # Scenario: Verify that user is able to create declaration for declaration type import-C21_J successfully
    #     Given I click on manage menu on the application header
    #     And   I click on chief declarations link
    #     And   Chief declaration page opens up successfully
    #     And   I click on create declaration button
    #     When  I enter reference details
    #         | EntryType | AdditionalType | RepType | InventoryReference | GrossWeight | Packages |
    #         | IM        | C21_J          | 2       |                    | 1234.5      | 10       |
    #     And I enter consignor details
    #         | Consignor        | Address1  | Address2    | City   | State | PostCode | Country | Eori |
    #         | tv new consignor | 401 NORTH | LAKE STREET | LONDON | utah  | WC2N 5DU | US      |      |
    #     And I enter Consignee details1
    #         | Consignee | Address1 | Address2 | City | State | PostCode | Country | Eori           |
    #         | CCCC      | A1A1     | A2A2A    | CITY | STATE | PCODE    | GB      | GB445809527006 |
    #     And I enter agent details
    #         | AgentName | Address1 | Address2 | City   | State   | PostCode | Country | Eori           |
    #         | AGENT1    | C        | AGADRS2  | AGCITY | AGSTATE | AGPCODE  | US      | GB417141383000 |
    #     And I click next button
    #     # And I enter transport details
    #     #     | CWC | Vessel                     | Flag | Location | TOT |
    #     #     | CN  | 2115RETESTING BY MASTHANBS | PA   | GBSOU    | 1   |
    #     And   I enter custom details
    #         | DUCRPartNo | DeferType1 | DeferNo1 | InvoiceAmount | Currency | VATAdjustment | CurrencyVAT |
    #         | T          | B          | 8104523  | 11,111        | GBP      | 222.20        | GBP         |
    #     And I click next button to go to item info page
    #     And I click add new item button
    #     And I enter terrif details
    #         | TerrifCode | TerrifDescription |
    #         | 6109100010 | T-shirt           |
    #     And I enter quantities
    #         | Value     | StatisticalValue | Qty1   | Qty2   |
    #         | 11,111 | 3                | 222.20 | 333.30 |
    #     And I enter item details
    #         | Adjustment | Percentage | Cpc     | Coo | Preference | ValMethod |
    #         | B          | 0          | 4000000 | CN  | 100        | 1         |
    #     And I enter additional Information
    #         | Code  | StatementText |
    #         | LIC99 | 3             |
    #     And I add previous documents
    #         | Class | Type | Reference |
    #         | Z     | 380  | test      |
    #     And I enter Packages
    #         | Count | PackageKind | Marks |
    #         | 10    | CT          | test  |
    #     And I enter documents
    #         | Code | Status |
    #         | N934 | AE     |
    #     And I enter Tax Lines
    #         | Code | Trid | Mop |
    #         | A00  | F    | F   |
    #         | B00  | S    | F   |
    #     And I enter supervising office details
    #         | SupervisingOffice | Address1 | Address2 | City | State   | PostCode | Country |
    #         | AGENT1            | AA1      | AA2      | CITY | AGSTATE | AGPCODE  | US      |
    #     And I click Add button to add item
    #     And I click next button to go to summary page
    #     And I click on submit button to submit declaration
    #     And I enter entry reference number in search entry on chief dashboard
    #     When  I click on Search button on chief dashboard
    #     Then  Declaration status should be as 'Entry Accepted'

    @create-declaration @import @smoke-front-end @WRD
    Scenario: Verify that user is able to create declaration for declaration type import-WRD_A successfully
        Given I click on manage menu on the application header
        And   I click on chief declarations link
        And   Chief declaration page opens up successfully
        And   I click on create declaration button
        When  I enter reference details
            | EntryType | AdditionalType | RepType | InventoryReference | GrossWeight | Packages |
            | IM        | WRD_A          | 2       |                    | 1234.5      | 10       |
        And I enter consignor details
            | Consignor        | Address1  | Address2    | City   | State | PostCode | Country | Eori |
            | tv new consignor | 401 NORTH | LAKE STREET | NEENAH | utah  | 54956    | US      |      |
        And I enter Consignee details1
            | Consignee | Address1 | Address2 | City | State | PostCode | Country | Eori           |
            | CCCC      | A1A1     | A2A2A    | CITY | STATE | PCODE    | GB      | GB445809527006 |
        And I enter agent details
            | AgentName | Address1 | Address2 | City   | State   | PostCode | Country | Eori           |
            | AGENT1    | C        | AGADRS2  | AGCITY | AGSTATE | AGPCODE  | US      | GB417141383000 |
        And I enter warehouse details
            | WarehouseName | Address1 | Address2 | City   | State   | PostCode | Country | WarehouseID |
            | AGENT1        | C        | AGADRS2  | AGCITY | AGSTATE | AGPCODE  | US      | E1923447GB  |
        And I enter supervising office details on party info screen
            | SupervisingOffice | Address1  | Address2    | City   | State   | PostCode | Country |
            | DPWORLD           | 401 NORTH | LAKE STREET | NEENAH | AGSTATE | 54956    | US      |
        And I click next button
        And I enter transport details for declaration type Import WRD & C21_K
            | CWC |
            | US  |
        And I enter custom details for declaration type Import WRD
            | DeferType1 | DeferNo1 | InvoiceAmount | Currency | FreightCharges | CurrencyFreight | Insurance | CurrencyInsurance | OtherChgs | CurrencyOtherChgs | VATAdjustment | CurrencyVAT |
            | B          | 8104523  | 10000         | GBP      | 100            | GBP             | 100       | GBP               | 222.20    | GBP               | 222           | GBP         |
        And I click next button to go to item info page
        And I click add new item button
        And I enter terrif details
            | TerrifCode | TerrifDescription |
            | 4803009000 | T-shirt           |
        And I enter quantities for declaration type import-WRD_A
            | Value     | StatisticalValue | Qty1   |
            | 10,000.00 |                  | 222.20 |
        And I enter item details
            | Adjustment | Percentage | Cpc     | Coo | Preference | ValMethod |
            | A          | 0          | 4022065 | US  | 100        | 1         |
        And I enter additional Information
            | Code  | StatementText |
            | GEN53 | 08/12/2020    |
            | LIC99 |               |
        And I add previous documents
            | Class | Type | Reference |
            | Z     | 380  | test      |
        And I enter Packages
            | Count | PackageKind | Marks |
            | 2     | PK          | test  |
        And I enter documents
            | Code | Status |
            | N934 | JE     |
        And I enter Tax Lines
            | Code | Trid | Mop |
            | B00  | S    | F   |
        And I click Add button to add item
        And I click next button to go to summary page
        And I click on submit button to submit declaration
        And I enter entry reference number in search entry on chief dashboard
        When  I click on Search button on chief dashboard
        Then  Declaration status should be as 'Entry Accepted'

    @cancel-declaration @import @smoke-front-end @WRD
    Scenario: Verify that user is able to cancel export declaration for C88_D
        Given I click on manage menu on the application header
        And   I click on chief declarations link
        And   Chief declaration page opens up successfully
        And   I enter entry reference number in search entry on chief dashboard
        When  I click on Search button on chief dashboard
        And   I click on actions button
        And   I click on cancel button
        And   I enter cancellation reason as 'cancellation reason'
        And   I click cancel submit button
        And   I again click confirm submit button
        Then  Declaration status should be as 'Cancellation Submitted'

    @create-declaration @import @smoke-front-end @C21_K
    Scenario: Verify that user is able to create declaration for Pre-logged declaration type import-C21_K successfully
        Given I click on manage menu on the application header
        And   I click on chief declarations link
        And   Chief declaration page opens up successfully
        And   I click on create declaration button
        When  I enter reference details
            | EntryType | AdditionalType | RepType | InventoryReference | GrossWeight | Packages |
            | IM        | C21_K          | 2       |                    | 1234.5      | 1        |
        And I enter consignor details
            | Consignor                  | Address1  | Address2    | City   | State | PostCode | Country | Eori |
            | KIMBERLY CLARK CORPORATION | 401 NORTH | LAKE STREET | NEENAH | utah  | 54956    | US      |      |
        And I enter Consignee details1
            | Consignee | Address1  | Address2 | City  | State | PostCode  | Country | Eori           |
            | CCCC      | 126 NORTH | STREET   | TEXAS | STATE | T 0086 76 | US      | GB445809527006 |
        And I enter agent details
            | AgentName | Address1 | Address2 | City   | State   | PostCode | Country | Eori           |
            | AGENT1    | C        | AGADRS2  | AGCITY | AGSTATE | AGPCODE  | US      | GB417141383000 |
        And I enter supervising office details on party info screen
            | SupervisingOffice | Address1  | Address2    | City   | State   | PostCode | Country |
            | DPWORLD           | 401 NORTH | LAKE STREET | NEENAH | AGSTATE | 54956    | US      |
        And I click next button
        And I enter transport details for declaration type Import WRD & C21_K
            | CWC | Location |
            |     | GBLON    |
        And I enter custom details
            | DUCRPartNo | DeferType1 | DeferNo1 | InvoiceAmount | Currency | VATAdjustment | CurrencyVAT |
            |            | B          | 8104523  |               |          |               |             |
        And I click next button to go to item info page
        And I click add new item button
        And I enter terrif details
            | TerrifCode | TerrifDescription |
            |            | PAPER PRODUCT     |
        And I enter item details
            | Adjustment | Percentage | Cpc     | Coo | Preference | ValMethod |
            |            |            | 0002090 |     |            |           |
        And I enter additional Information
            | Code  | StatementText |
            | GEN53 | 08/12/2020    |
            | LIC99 |               |
        And I add previous documents
            | Class | Type | Reference |
            | Z     | 380  | 90639992  |
        And I enter Packages
            | Count | PackageKind | Marks   |
            | 2     | PK          | PKGCFO1 |
        And I enter documents
            | Code | Status | Reference | Part | Reason |
            | Y020 | JP     | ABCD      | 40   | Reason |
        And I enter Tax Lines for declaration type Import C21_K
            | Code | Trid | TaxAmount | DeclaredTaxAmount | Mop |
            | B00  |      | 1.00      | 1.00              | F   |
        And I click Add button to add item
        And I click next button to go to summary page
        And I click on submit button to submit declaration
        And I enter entry reference number in search entry on chief dashboard
        When  I click on Search button on chief dashboard
        Then  Declaration status should be as 'Entry Accepted'

    @create-declaration @import @smoke-front-end
    Scenario: Verify that user is able to Amend Pre-lodged declaration type C88 and submit it
        Given I click on manage menu on the application header
        And I click on chief declarations link
        And Chief declaration page opens up successfully
        And I enter entry reference number in search entry on chief dashboard
        When I click on Search button on chief dashboard
        And I click on Amend button
        And I amend the consignee details
            | Consignee | Address1 | Address2 | City | State | PostCode | Country | Eori |
            | CCCC      |          |          |      |       |          |         |      |
        And I enter the ammendment reason and submit
        And I enter entry reference number in search entry on chief dashboard
        And I click on Search button on chief dashboard
        Then Declaration status should be as 'Amendment Accepted'

    @create-declaration @import @smoke-front-end @C21_J
    Scenario: Verify that user is able to create declaration for After Arrival declaration type import-C21_J successfully
        Given I click on manage menu on the application header
        And   I click on chief declarations link
        And   Chief declaration page opens up successfully
        And   I click on create declaration button
        When  I enter reference details
            | EntryType | AdditionalType | RepType | InventoryReference | GrossWeight | Packages |
            | IM        | C21_J          | 2       |                    | 1234.5      | 1        |
        And I enter consignor details
            | Consignor                  | Address1  | Address2    | City   | State | PostCode | Country | Eori |
            | KIMBERLY CLARK CORPORATION | 401 NORTH | LAKE STREET | NEENAH | utah  | 54956    | US      |      |
        And I enter Consignee details1
            | Consignee | Address1  | Address2 | City  | State | PostCode  | Country | Eori           |
            | CCCC      | 126 NORTH | STREET   | TEXAS | STATE | T 0086 76 | US      | GB445809527006 |
        And I enter agent details
            | AgentName | Address1 | Address2 | City   | State   | PostCode | Country | Eori           |
            | AGENT1    | C        | AGADRS2  | AGCITY | AGSTATE | AGPCODE  | US      | GB417141383000 |
        And I enter supervising office details on party info screen
            | SupervisingOffice | Address1  | Address2    | City   | State   | PostCode | Country |
            | DPWORLD           | 401 NORTH | LAKE STREET | NEENAH | AGSTATE | 54956    | US      |
        And I click next button
        And I enter transport details for declaration type Import WRD & C21_K
            | CWC | Location |
            |     | GBLON    |
        And I enter custom details
            | DUCRPartNo | DeferType1 | DeferNo1 | InvoiceAmount | Currency | VATAdjustment | CurrencyVAT |
            |            | B          | 8104523  |               |          |               |             |
        And I click next button to go to item info page
        And I click add new item button
        And I enter terrif details
            | TerrifCode | TerrifDescription |
            |            | PAPER PRODUCT     |
        And I enter item details
            | Adjustment | Percentage | Cpc     | Coo | Preference | ValMethod |
            |            |            | 0002090 |     |            |           |
        And I enter additional Information
            | Code  | StatementText |
            | GEN53 | 08/12/2020    |
            | LIC99 |               |
        And I add previous documents
            | Class | Type | Reference |
            | Z     | 380  | 90639992  |
        And I enter Packages
            | Count | PackageKind | Marks   |
            | 2     | PK          | PKGCFO1 |
        And I enter documents
            | Code | Status | Reference | Part | Reason |
            | Y020 | JP     | ABCD      | 40   | Reason |
        And I enter Tax Lines for declaration type Import C21_K
            | Code | Trid | TaxAmount | DeclaredTaxAmount | Mop |
            | B00  |      | 1.00      | 1.00              | F   |
        And I click Add button to add item
        And I click next button to go to summary page
        And I click on submit button to submit declaration
        And I enter entry reference number in search entry on chief dashboard
        When  I click on Search button on chief dashboard
        Then  Declaration status should be as 'Entry Accepted'

    @create-declaration @import @smoke-front-end
    Scenario: Verify that user is able to Amend After Arrival Import declaration type and submit it
        Given I click on manage menu on the application header
        And I click on chief declarations link
        And Chief declaration page opens up successfully
        And I enter entry reference number in search entry on chief dashboard
        When I click on Search button on chief dashboard
        And I click on Amend button
        And I amend the consignee details
            | Consignee | Address1 | Address2 | City | State | PostCode | Country | Eori |
            | CCCC      |          |          |      |       |          |         |      |
        And I enter the ammendment reason and submit
        And I enter entry reference number in search entry on chief dashboard
        And I click on Search button on chief dashboard
        Then Declaration status should be as 'Amendment Accepted'

    @create-declaration @smoke-front-end
    Scenario: Verify that user is able to Search Declaration by status criteria
        Given I click on manage menu on the application header
        And I click on chief declarations link
        And Chief declaration page opens up successfully
        And I enter entry reference number in search entry on chief dashboard
        And I enter the status criteria as 'Amendment Accepted'
        When I click on Search button on chief dashboard
        Then Declaration status should be as 'Amendment Accepted'
