Feature: Cancel Draft Declaration

    Background: Login to the application
        Given I go to application
        When  I enter valid credentials
            | username    | password    |
            | autoukadmin | Admin@12345 |
        And I click on login button
        Then Application home page opens successfully

    @cancel-draft-declaration @smoke-front-end
    Scenario: Verify that user is able to cancel the draft declartion
        Given I click on manage menu on the application header
        And   I click on chief declarations link
        And   Chief declaration page opens up successfully
        And   I click on create declaration button
        And   I enter reference details for draft cancellation
        When  I click on save as draft button
        And   I enter entry reference number in search entry on chief dashboard
        And   I click on Search button on chief dashboard
        And   I click on actions button
        And   I click on cancel button to cancel draft
        And   I click on delete button
        And   I enter entry reference number in search entry on chief dashboard
        And   I click on Search button on chief dashboard
        Then  Draft declaration should deleted


