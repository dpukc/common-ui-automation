Feature: Query Declaration Status and Orignal Input

    Background: Login to the application
        Given I go to application
        When  I enter valid credentials
            | username    | password    |
            | autoukadmin | Admin@12345 |
        And I click on login button
        Then Application home page opens successfully

    @query-declaration @query-declaration-status @smoke-front-end
    Scenario: Verify that user is able to query the updated status of declration with chief (DEC Query)
        Given I click on manage menu on the application header
        And   I click on chief declarations link
        And   Chief declaration page opens up successfully
        And   I click on create declaration button
        And  I enter reference details for C21E_K
            | EntryType | AdditionalType | RepType | Mucr | GrossWeight | Packages |
            | EX        | C21E_K         | 2       |      |             | 10       |
        And I enter consignor details for C21E_K
            | Consignor        | Address1  | Address2    | City   | State | PostCode | Country | Eori           |
            | tv new consignor | 401 NORTH | LAKE STREET | LONDON | utah  | WC2N 5DU | GB      | GB174313477000 |
        And I enter Consignee details1 for C21E_K
            | Consignee | Address1 | Address2 | City | State | PostCode | Country | Eori           |
            | CCCC      | A1A1     | A2A2A    | CITY | STATE | PCODE    | US      | GB445809527006 |
        And I enter agent details for C21E_K
            | AgentName | Address1 | Address2 | City   | State   | PostCode | Country | Eori           |
            | AGENT1    | C        | AGADRS2  | AGCITY | AGSTATE | AGPCODE  | GB      | GB615081466000 |
        And I click next button
        And I enter transport details for C21E_K
            | DispatchCountry | DestinationCountry | Vessel | Flag | Location | TOT |
            |                 |                    |        |      | GBDEU    |     |
        And   I enter custom details for C21E_K
            | DUCRPartNo | DeferType1 | DeferNo1 | InvoiceAmount | Currency | VATAdjustment | CurrencyVAT |
            | D          | B          | 8104523  | 11,111.10     | GBP      | 222.20        | GBP         |
        And I click next button to go to item info page
        And I click add new item button
        And I enter terrif details for C21E_K
            | TerrifCode | TerrifDescription |
            | 6109100010 | T-shirt           |
        And I enter item details for C21E_K
            | Adjustment | Percentage | Cpc     | Coo | Preference | ValMethod |
            |            |            | 1000041 |     |            |           |
        And I add previous documents for C21E_K
            | Class | Type | Reference |
            | Z     | 380  | 80790     |
        And I enter Packages for C21E_K
            | Count | PackageKind | Marks |
            | 10    | PK          | ADD/D |
        And I enter documents for C21E_K
            | Code | Status | Reference |
            | N825 | JE     | 34242     |
        And I click Add button to add item
        And I click next button to go to summary page
        And I click on submit button to submit declaration
        And I enter entry reference number in search entry on chief dashboard
        And  I click on Search button on chief dashboard
        And  Declaration status should be as 'Entry Accepted'
        And   I click on actions button
        When  I click on query button
        And   I select query type as 'DEC'
        And   I click submit button
        And   I click on actions button
        And   I click on view
        And   I click on messages tab
        Then  'DEC' query response should be received

    @query-declaration @query-declaration-orignal-input @smoke-front-end
    Scenario: Verify that user is able to query the orignal input of declration (DEVD Query)
        Given I click on manage menu on the application header
        And   I click on chief declarations link
        And   Chief declaration page opens up successfully
        And   I enter entry reference number in search entry on chief dashboard
        And   I click on Search button on chief dashboard
        And   I click on actions button
        When  I click on query button
        And   I select query type as 'DEVD'
        And   I click submit button
        And   I click on actions button
        And   I click on view
        And   I click on messages tab
        Then  'DEVD' query response should be received



