Feature: Query Declaration Status and Orignal Input

    Background: Login to the application
        Given I go to application
        When  I enter valid credentials
        And I click on login button
        Then Application home page opens successfully

    @printed-reports @smoke-front-end
    Scenario: Verify that user is able to generate and view  DTI-H2 report in reports section for C21_K declaration
        Given I click on manage menu on the application header
        And   I click on chief declarations link
        And   Chief declaration page opens up successfully
        And   I click on create declaration button
        And  I enter reference details
            | EntryType | AdditionalType | RepType | InventoryReference | GrossWeight | Packages |
            | IM        | C21_K          | 2       |                    | 1234.5      | 1        |
        And I enter consignor details
            | Consignor                  | Address1  | Address2    | City   | State | PostCode | Country | Eori |
            | KIMBERLY CLARK CORPORATION | 401 NORTH | LAKE STREET | NEENAH | utah  | 54956    | US      |      |
        And I enter Consignee details1
            | Consignee | Address1  | Address2 | City  | State | PostCode  | Country | Eori           |
            | CCCC      | 126 NORTH | STREET   | TEXAS | STATE | T 0086 76 | US      | GB445809527006 |
        And I enter agent details
            | AgentName | Address1 | Address2 | City   | State   | PostCode | Country | Eori           |
            | AGENT1    | C        | AGADRS2  | AGCITY | AGSTATE | AGPCODE  | US      | GB417141383000 |
        And I enter supervising office details on party info screen
            | SupervisingOffice | Address1  | Address2    | City   | State   | PostCode | Country |
            | DPWORLD           | 401 NORTH | LAKE STREET | NEENAH | AGSTATE | 54956    | US      |
        And I click next button
        And I enter transport details for declaration type Import WRD & C21_K
            | CWC | Location |
            |     | GBLON    |
        And I enter custom details
            | DUCRPartNo | DeferType1 | DeferNo1 | InvoiceAmount | Currency | VATAdjustment | CurrencyVAT |
            |            | B          | 8104523  |               |          |               |             |
        And I click next button to go to item info page
        And I click add new item button
        And I enter terrif details
            | TerrifCode | TerrifDescription |
            |            | PAPER PRODUCT     |
        And I enter item details
            | Adjustment | Percentage | Cpc     | Coo | Preference | ValMethod |
            |            |            | 0002090 |     |            |           |
        And I enter additional Information
            | Code  | StatementText |
            | GEN53 | 08/12/2020    |
            | LIC99 |               |
        And I add previous documents
            | Class | Type | Reference |
            | Z     | 380  | 90639992  |
        And I enter Packages
            | Count | PackageKind | Marks   |
            | 2     | PK          | PKGCFO1 |
        And I enter documents
            | Code | Status | Reference | Part | Reason |
            | Y020 | JP     | ABCD      | 40   | Reason |
        And I enter Tax Lines for declaration type Import C21_K
            | Code | Trid | TaxAmount | DeclaredTaxAmount | Mop |
            | B00  |      | 1.00      | 1.00              | F   |
        And I click Add button to add item
        And I click next button to go to summary page
        And I click on submit button to submit declaration
        And I enter entry reference number in search entry on chief dashboard
        When  I click on Search button on chief dashboard
        Then  Declaration status should be as 'Entry Accepted'
        And   I click on actions button
        And   I click on view
        When  I click on reports tab
        And   I click on dt1-h2 view report button to view report
        Then  The report should be open and contain data



