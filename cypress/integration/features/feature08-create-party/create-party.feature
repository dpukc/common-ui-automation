Feature: Create Party

    Background: Login to the application
        Given I go to application
        When  I enter valid credentials
            | username      | password   |
            | customukadmin | Admin@1234 |
        And I click on login button
        Then Application home page opens successfully

    @smoke-front-end @create-party
    Scenario: Verify that user is able to create party using party directory service
        Given I click on manage menu on the application header
        And   I click on party directory link
        And   Party directory  page opens up successfully
        And   I click on add party button
        When  I enter party details
            | SelectParty | PartyName | State | PostCode | Country | Eori      |
            | Consignee   | testAuto  | CA    | 54000    | US      | GB1234359 |
        And   I click on save button on add party screen
        And   I click on OK button on confirmation pop up
        And   I enter short code to filter search results
        And   I click on Search button
        Then  New party is created and appears on party list