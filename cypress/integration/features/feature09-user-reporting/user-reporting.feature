Feature: User Reporting

    Background: Login to the application
        Given I go to application
        When  I enter valid credentials
            | username      | password   |
            | customukadmin | Admin@1234 |
        And I click on login button
        Then Application home page opens successfully

    @smoke-front-end @user-reporting
    Scenario Outline: Verify that user is able to generate import entry breakdown report
        Given I click on manage menu on the application header
        And   I click on chief declarations link
        And   Chief declaration page opens up successfully
        And   I click on user reporting button
        When  I enter report name "<ReportName>"
        And   I click on run report button
        And   I click on "<ReportType>" button
        # Then  Report file should be downloaded

        Examples:
            | ReportName             | ReportType |
            | Import Entry Breakdown | csv        |
            | Export Entry Breakdown | excel      |