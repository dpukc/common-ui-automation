Feature: create transit declaration

      Background: Login to the application
            Given I go to application
            When  I enter valid credentials
            And I click on login button
            Then Application home page opens successfully

      # # T1 Type declaration Dispatch : XI- Norther Island, Destination : GB Uk, Only Manadatory fields are adedd
      @create-transit-normal-declaration @smoke-front-end @transit @T1Declaration @EUToNONEU
      Scenario: Verify that user is able to create T1 Type Transit Declaration when Dispatch from EU Country to NonEU Country
            Given I click on manage menu on the application header
            And   I click on transit declarations link
            And   transit declaration page opens up successfully
            And   I click on create declaration button
            And   I enter reference details for Transit declaration
                  | DeclarationType | DeclarationPlace |
                  | T1              | Belfast          |
            And   I enter principal details for Transit declaration
                  | Principalname    | StreetNumber1 | StreetNumber2 | City    | PostCode | Country | Eori           |
                  | NI CONSIGNOR LTD | address1111   | address2      | BELFAST | BT1 1AA  | XI      | XI195624547845 |
            And   I enter consignor details for Transit declaration
                  | Consignorname    | StreetNumber1 | StreetNumber2 | City    | PostCode | Country | Eori           |
                  | NI CONSIGNOR LTD | address1111   | address2      | BELFAST | BT1 1AA  | XI      | XI195624547845 |
            And   I enter consignee details for Transit declaration
                  | Consigneename         | StreetNumber1   | StreetNumber2     | City            | PostCode | Country | Eori           |
                  | NCTS UK TEST LAB HMCE | 11TH FLOOR ALEX | HOUSE VICTORIA AV | SOUTHEND-ON-SEA | SS99 1AA | GB      | GN954131533000 |
            And   I click on next button
            And   I enter dispatchdestination details on headerinfo stepper for Transit declaration
                  | Countryofdispatch | ReferenceOfficeOfDeparture | Countryofdestination | ReferenceOfficeOfDestination | CountryofTermination | ReferenceOfficeOfTermination |
                  | XI                | XI000142                   | GB                   | GB000060                     | GB                   | GB000060                     |
            And   I enter transit office details for transit declaration
                  | TrasitOfficeReference |
                  | GB000122              |
            And   I enter goods total for transit declaration
                  | TotalNumberOfItem | TotalGrossMass | TotalNumberOfPackagesPieces |
                  | 1                 | 100            | 10                          |
            And   I enter goods location details for transit declaration
                  | CustomSubPlace |
                  | Belfast ERTS   |
            And   I enter transport details for transit declaration
                  | IdentityMeansOfTransportAtDeparture | NationalityMeansOfTransportAtDeparture |
                  | Auto 15 REG                         | XI                                     |
            And   I enter guarantee details for transit declaration
                  | GuaranteeReferenceNumber | Accesscode |
                  | 20XI00014200002V8        | AC01       |
            And   I click on next button
            And   I click on add item button for transit declaration
            And   I enter item particular details for transit declaration
                  | CommodityCode | GrossMass | NetMass | ItemDescription | ItemValue |
                  | 1001190012    | 100       | 99.99   | Whole Wheat     | 8500      |
            And   I enter package details for transit declaration
                  | KindOfPackage | NumberOfPackage | MarksAndNumberOfPackages |
                  | BX            | 10              | AB234                    |
            And   I enter CAL as special mention details for transit declaration
                  | AdditionalInforamtionCode |
                  | CAL                       |
            And   I click on add item icon for transit declaration
            And   I click on next button
            And   I click on submit button to submit transit declaration
            And   I enter entry reference number in search entry on transit dashboard
            When  I click on Search button on transit dashboard
            Then  Declaration status should be as 'Declaration Submitted' for transit declaration

      # T1 Type declaration Dispatch :GB- UK, Destination : IT, Cargo container, Seal Number, Item-> Produce document, Number of pieces
      @create-transit-declaration @smoke-front-end @transit @T1Declaration @NONEUToEU
      Scenario: Verify that user is able to create T1 Type Transit Declaration when Dispatch from NonEU Country to EU Country
            Given I click on manage menu on the application header
            And   I click on transit declarations link
            And   transit declaration page opens up successfully
            And   I click on create declaration button
            And   I enter reference details for Transit declaration
                  | DeclarationType | DeclarationPlace |
                  | T1              | Dover            |
            And   I enter principal details for Transit declaration
                  | Principalname         | StreetNumber1   | StreetNumber2     | City             | PostCode | Country | Eori           |
                  | NCTS UK LAB PRINCIPAL | 11th floor alex | house,victoria av | southend-on-sea, | SS99 1AA | GB      | GB954131533000 |
            And   I enter consignor details for Transit declaration
                  | Consignorname         | StreetNumber1   | StreetNumber2     | City             | PostCode | Country | Eori           |
                  | NCTS UK LAB PRINCIPAL | 11th floor alex | house,victoria av | southend-on-sea, | SS99 1AA | GB      | GB954131533000 |
            And   I enter consignee details for Transit declaration
                  | Consigneename | StreetNumber1  | StreetNumber2 | City     | PostCode | Country | Eori          |
                  | CORLEONE      | 23, LE DON STR | address2      | CORLEONE | 123-456  | IT      | IT17THEBOSS42 |
            And   I click on next button
            And   I enter dispatchdestination details on headerinfo stepper for Transit declaration
                  | Countryofdispatch | ReferenceOfficeOfDeparture | Countryofdestination | ReferenceOfficeOfDestination | CountryofTermination | ReferenceOfficeOfTermination |
                  | GB                | GB000060                   | IT                   | IT018105                     | IT                   | IT018105                     |
            And   I enter transit office details for transit declaration
                  | TrasitOfficeReference |
                  | FR001260              |
            And   I enter goods total for transit declaration
                  | TotalNumberOfItem | TotalGrossMass | TotalNumberOfPackagesPieces |
                  | 1                 | 100            | 100                         |
            And   I enter cargo container at header info stepper for transit declaration
                  | CargoContainerNumber |
                  | AutomationCargo1     |
            And   I enter seal details for transit declaration
                  | SealNumber      |
                  | AutomationSeal1 |
            And   I enter goods location details for transit declaration
                  | CustomSubPlace |
                  | Dover ERTS     |
            And   I enter transport details for transit declaration
                  | IdentityMeansOfTransportAtDeparture | NationalityMeansOfTransportAtDeparture |
                  | Auto 15 REG                         | GB                                     |
            And   I enter guarantee details for transit declaration
                  | GuaranteeReferenceNumber | Accesscode |
                  | 20XI00014200002V8        | AC01       |
            And   I click on next button
            And   I click on add item button for transit declaration
            And   I enter item particular details for transit declaration
                  | CommodityCode | GrossMass | NetMass | ItemDescription | ItemValue |
                  | 1001190012    | 100       | 99.99   | Whole Wheat     | 8500      |
            And   I enter peices details in package setion intem info stepper for transit declaration
                  | KindOfPackage | NumberOfPeices |
                  | NE            | 100            |
            And   I enter cargo container at item info stepper for transit declaration
                  | CargoContainerNumber |
                  | AutomationCargo1     |
            And   I enter CAL as special mention details for transit declaration
                  | AdditionalInforamtionCode |
                  | CAL                       |
            And   I enter produce document certificate type for transit declaration
                  | ProduceDocumentCertificateType | ProduceDocumentCertificateReference |
                  | 2                              | Automation Produce Doc              |
            And   I enter previous administrative references for transit Declaration
                  | PreviousDocumentCertificateType | PreviousDocumentCertificateReference |
                  | T2                              | AutomationTestDoc                    |
            And   I click on add item icon for transit declaration
            And   I click on next button
            And   I click on submit button to submit transit declaration
            And   I enter entry reference number in search entry on transit dashboard
            When  I click on Search button on transit dashboard
            Then  Declaration status should be as 'Declaration Submitted' for transit declaration

      # T2 Type declaration Dispatch :GB- UK, Destination : IT, mandatory fields , 2items
      @create-transit-declaration @smoke-front-end @transit @T2Declaration @NONEUToEU
      Scenario: Verify that user is able to create T2 Type Transit Declaration when Dispatch from NonEU Country to EU Country
            Given I click on manage menu on the application header
            And   I click on transit declarations link
            And   transit declaration page opens up successfully
            And   I click on create declaration button
            And   I enter reference details for Transit declaration
                  | DeclarationType | DeclarationPlace |
                  | T2              | Dover            |
            And   I enter principal details for Transit declaration
                  | Principalname         | StreetNumber1   | StreetNumber2     | City             | PostCode | Country | Eori           |
                  | NCTS UK LAB PRINCIPAL | 11th floor alex | house,victoria av | southend-on-sea, | SS99 1AA | GB      | GB954131533000 |
            And   I enter consignor details for Transit declaration
                  | Consignorname         | StreetNumber1   | StreetNumber2     | City             | PostCode | Country | Eori           |
                  | NCTS UK LAB PRINCIPAL | 11th floor alex | house,victoria av | southend-on-sea, | SS99 1AA | GB      | GB954131533000 |
            And   I enter consignee details for Transit declaration
                  | Consigneename | StreetNumber1  | StreetNumber2 | City     | PostCode | Country | Eori          |
                  | CORLEONE      | 23, LE DON STR | address2      | CORLEONE | 123-456  | IT      | IT17THEBOSS42 |
            And   I click on next button
            And   I enter dispatchdestination details on headerinfo stepper for Transit declaration
                  | Countryofdispatch | ReferenceOfficeOfDeparture | Countryofdestination | ReferenceOfficeOfDestination | CountryofTermination | ReferenceOfficeOfTermination |
                  | GB                | GB000060                   | IT                   | IT018105                     | IT                   | IT018105                     |
            And   I enter transit office details for transit declaration
                  | TrasitOfficeReference |
                  | FR001260              |
            And   I enter goods total for transit declaration
                  | TotalNumberOfItem | TotalGrossMass | TotalNumberOfPackagesPieces |
                  | 1                 | 100            | 10                          |
            And   I enter goods location details for transit declaration
                  | CustomSubPlace |
                  | Dover ERTS     |
            And   I enter transport details for transit declaration
                  | IdentityMeansOfTransportAtDeparture | NationalityMeansOfTransportAtDeparture |
                  | Auto 15 REG                         | GB                                     |
            And   I enter guarantee details for transit declaration
                  | GuaranteeReferenceNumber | Accesscode |
                  | 20XI00014200002V8        | AC01       |
            And   I click on next button
            And   I click on add item button for transit declaration
            And   I enter item particular details for transit declaration
                  | CommodityCode | GrossMass | NetMass | ItemDescription | ItemValue |
                  | 1001190012    | 100       | 99.99   | Whole Wheat     | 8500      |
            And   I enter package details for transit declaration
                  | KindOfPackage | NumberOfPackage | MarksAndNumberOfPackages |
                  | BX            | 10              | AB234                    |
            And   I enter CAL as special mention details for transit declaration
                  | AdditionalInforamtionCode |
                  | CAL                       |
            And   I enter previous administrative references for transit Declaration
                  | PreviousDocumentCertificateType | PreviousDocumentCertificateReference |
                  | T2                              | AutomationTestDoc                    |
            And   I click on add item icon for transit declaration
            And   I click on next button
            And   I click on submit button to submit transit declaration
            And   I enter entry reference number in search entry on transit dashboard
            When  I click on Search button on transit dashboard
            Then  Declaration status should be as 'Declaration Submitted' for transit declaration

      # T2 Type declaration Dispatch :XI- northern island, Destination : GB, mandatory fields , item->previous administation document
      @create-transit-declaration @smoke-front-end @transit @T2Declaration @EUToNONEU
      Scenario: Verify that user is able to create T2 Type Transit Declaration when Dispatch from EU Country to NonEU Country
            Given I click on manage menu on the application header
            And   I click on transit declarations link
            And   transit declaration page opens up successfully
            And   I click on create declaration button
            And   I enter reference details for Transit declaration
                  | DeclarationType | DeclarationPlace |
                  | T2              | Belfast          |
            And   I enter principal details for Transit declaration
                  | Principalname    | StreetNumber1 | StreetNumber2 | City    | PostCode | Country | Eori           |
                  | NI CONSIGNOR LTD | address1111   | address2      | BELFAST | BT1 1AA  | XI      | XI195624547845 |
            And   I enter consignor details for Transit declaration
                  | Consignorname    | StreetNumber1 | StreetNumber2 | City    | PostCode | Country | Eori           |
                  | NI CONSIGNOR LTD | address1111   | address2      | BELFAST | BT1 1AA  | XI      | XI195624547845 |
            And   I enter consignee details for Transit declaration
                  | Consigneename         | StreetNumber1   | StreetNumber2     | City            | PostCode | Country | Eori           |
                  | NCTS UK TEST LAB HMCE | 11TH FLOOR ALEX | HOUSE VICTORIA AV | SOUTHEND-ON-SEA | SS99 1AA | GB      | GN954131533000 |
            And   I click on next button
            And   I enter dispatchdestination details on headerinfo stepper for Transit declaration
                  | Countryofdispatch | ReferenceOfficeOfDeparture | Countryofdestination | ReferenceOfficeOfDestination | CountryofTermination | ReferenceOfficeOfTermination |
                  | XI                | XI000142                   | GB                   | GB000060                     | GB                   | GB000060                     |
            And   I enter transit office details for transit declaration
                  | TrasitOfficeReference |
                  | GB000122              |
            And   I enter goods total for transit declaration
                  | TotalNumberOfItem | TotalGrossMass | TotalNumberOfPackagesPieces |
                  | 2                 | 135.35         | 110                         |
            And   I enter goods location details for transit declaration
                  | CustomSubPlace |
                  | Belfast ERTS   |
            And   I enter transport details for transit declaration
                  | IdentityMeansOfTransportAtDeparture | NationalityMeansOfTransportAtDeparture |
                  | Auto 15 REG                         | XI                                     |
            And   I enter guarantee details for transit declaration
                  | GuaranteeReferenceNumber | Accesscode |
                  | 20XI00014200002V8        | AC01       |
            And   I click on next button
            And   I click on add item button for transit declaration
            And   I enter item particular details for transit declaration
                  | CommodityCode | GrossMass | NetMass | ItemDescription | ItemValue |
                  | 1001190012    | 100       | 99.99   | Whole Wheat     | 8500      |
            And   I enter peices details in package setion intem info stepper for transit declaration
                  | KindOfPackage | NumberOfPeices |
                  | NE            | 100            |
            And   I enter CAL as special mention details for transit declaration
                  | AdditionalInforamtionCode |
                  | CAL                       |
            And   I enter previous administrative references for transit Declaration
                  | PreviousDocumentCertificateType | PreviousDocumentCertificateReference |
                  | T2                              | AutomationTestDoc                    |
            And   I click on add item icon for transit declaration
            And   I click on add item button for transit declaration
            And   I enter item particular details for transit declaration
                  | CommodityCode | GrossMass | NetMass | ItemDescription     | ItemValue |
                  | 3801100030    | 35.35     | 35      | Artificial graphite | 2143      |
            And   I enter package details for transit declaration
                  | KindOfPackage | NumberOfPackage | MarksAndNumberOfPackages |
                  | BX            | 10              | AB234                    |
            And   I click on add item icon for transit declaration
            And   I click on next button
            And   I click on submit button to submit transit declaration
            And   I enter entry reference number in search entry on transit dashboard
            When  I click on Search button on transit dashboard
            Then  Declaration status should be as 'Declaration Submitted' for transit declaration