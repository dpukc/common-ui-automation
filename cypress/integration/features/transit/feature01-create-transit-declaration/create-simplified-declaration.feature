Feature: create transit normal declaration

      Background: Login to the application
            Given I go to application
            When  I enter valid credentials
            And I click on login button
            Then Application home page opens successfully

      # # T1 Type declaration Dispatch : XI- Norther Island, Destination : GB Uk, Only Manadatory fields are adedd
      @create-transit-simplified-declaration @smoke-front-end @transit @T1Declaration @EUToNONEU
      Scenario: Verify that user is able to create T1 Type Transit Declaration when Dispatch from EU Country to NonEU Country
            Given I click on manage menu on the application header
            And   I click on transit declarations link
            And   transit declaration page opens up successfully
            And   I click on create declaration button
            And   I click on simplified checkbox
            And   I enter reference details for Transit declaration
                  | DeclarationType | DeclarationPlace |
                  | T1              | Belfast          |
            And   I enter principal details for Transit declaration
                  | Principalname    | StreetNumber1 | StreetNumber2 | City    | PostCode | Country | Eori           |
                  | NI CONSIGNOR LTD | address1111   | address2      | BELFAST | BT1 1AA  | XI      | XI195624547845 |
            And   I enter consignor details for Transit declaration
                  | Consignorname    | StreetNumber1 | StreetNumber2 | City    | PostCode | Country | Eori           |
                  | NI CONSIGNOR LTD | address1111   | address2      | BELFAST | BT1 1AA  | XI      | XI195624547845 |
            And   I enter consignee details for Transit declaration
                  | Consigneename         | StreetNumber1   | StreetNumber2     | City            | PostCode | Country | Eori           |
                  | NCTS UK TEST LAB HMCE | 11TH FLOOR ALEX | HOUSE VICTORIA AV | SOUTHEND-ON-SEA | SS99 1AA | GB      | GN954131533000 |
            And   I click on next button
            And   I enter dispatchdestination details on headerinfo stepper for Transit declaration
                  | Countryofdispatch | ReferenceOfficeOfDeparture | Countryofdestination | ReferenceOfficeOfDestination | CountryofTermination | ReferenceOfficeOfTermination |
                  | XI                | XI000142                   | GB                   | GB000060                     | GB                   | GB000060                     |
            And   I enter transit office details for transit declaration
                  | TrasitOfficeReference |
                  | GB000122              |
            And   I enter goods total for transit declaration
                  | TotalNumberOfItem | TotalGrossMass | TotalNumberOfPackagesPieces |
                  | 1                 | 100            | 10                          |
            And   I enter seal details
                  | SealIdentifier |
                  | Seal1          |
            And   I enter goods location details for transit declaration for simp declaration
                  | AuthorizedLocationOfGoods | PlaceOfLoading |
                  | 195624547-XI00DEP         | Belfast ERTS   |
            And   I enter transport details for transit declaration
                  | IdentityMeansOfTransportAtDeparture | NationalityMeansOfTransportAtDeparture |
                  | Auto 15 REG                         | XI                                     |
            And   I enter guarantee details for transit declaration
                  | GuaranteeReferenceNumber | Accesscode |
                  | 20XI00014200002V8        | AC01       |
            And   I click on next button
            And   I click on add item button for transit declaration
            And   I enter item particular details for transit declaration
                  | CommodityCode | GrossMass | NetMass | ItemDescription | ItemValue |
                  | 1001190012    | 100       | 99.99   | Whole Wheat     | 8500      |
            And   I enter package details for transit declaration
                  | KindOfPackage | NumberOfPackage | MarksAndNumberOfPackages |
                  | BX            | 10              | AB234                    |
            And   I enter CAL as special mention details for transit declaration
                  | AdditionalInforamtionCode |
                  | CAL                       |
            And   I click on add item icon for transit declaration
            And   I click on next button
            And   I click on submit button to submit transit declaration
            And   I enter entry reference number in search entry on transit dashboard
            When  I click on Search button on transit dashboard
            Then  Declaration status should be as 'Declaration Submitted' for transit declaration

