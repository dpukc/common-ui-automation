Feature: Copy to new transit normal declaration

      Background: Login to the application
            Given I go to application
            When  I enter valid credentials
            And I click on login button
            Then Application home page opens successfully

      # # T1 Type declaration Dispatch : XI- Norther Island, Destination : GB Uk, Only Manadatory fields are adedd
      @copy-transit-normal-declaration @smoke-front-end @transit @T1Declaration @EUToNONEU
      Scenario: Verify that user is able to copy T1 Type Transit Declaration when Dispatch from EU Country to NonEU Country
            Given I click on manage menu on the application header
            And   I click on transit declarations link
            And   transit declaration page opens up successfully
            And   I enter entry reference number in search entry on transit dashboard for copy to new
            And   I click on Search button on transit dashboard
            And   I click action button
            And   I click copy to new button
            And   I enter reference details for Transit declaration for copy to new
            And   I click on next button
            And   I enter goods total for transit declaration for copy to new
                  | TotalNumberOfItem | TotalGrossMass | TotalNumberOfPackagesPieces |
                  | 1                 | 100            | 10                          |
            And   I enter access code in guarantee details for transit declaration
                  | Accesscode |
                  | AC01       |
            And   I click on next button on header info page
            And   I click on edit item button for transit declaration

            And   I enter item particular details for transit declaration for copy to new
                  | CommodityCode | GrossMass | NetMass | ItemDescription | ItemValue |
                  | 1001190012    | 100       | 99.99   | Whole Wheat     | 8500      |

            And   I click on save item icon for transit declaration
            And   I click on next button
            And   I click on submit button to submit transit declaration
            And   I enter entry reference number in search entry on transit dashboard
            When  I click on Search button on transit dashboard
            Then  Declaration status should be as 'Declaration Submitted' for transit declaration
