Feature: Cancel transit draft declaration

      Background: Login to the application
            Given I go to application
            When  I enter valid credentials
            And I click on login button
            Then Application home page opens successfully

      @cancel-transit-mrn-accepted-declaration @smoke-front-end @transit
      Scenario: Verify that user is able to cancel T1 Type Transit Declaration when Dispatch from EU Country to NonEU Country
            Given I click on manage menu on the application header
            And   I click on transit declarations link
            And   transit declaration page opens up successfully
            And   I enter entry reference number in search entry on transit dashboard
            And   I click on Search button on transit dashboard
            And   I click action button
            And   I click cancel button to cancel declaration
            And   I enter cancellation reason as "cancellation reason" for transit declaration
            And   I click submit button to cancel transit declaration
            When  I click delete button on confirmation popup
            Then  Declaration status should be "Cancellation Submitted"


