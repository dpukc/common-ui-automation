Feature: Cancel transit draft declaration

      Background: Login to the application
            Given I go to application
            When  I enter valid credentials
            And I click on login button
            Then Application home page opens successfully

      @cancel-transit-draft-declaration @smoke-front-end @transit @T1Declaration @EUToNONEU
      Scenario: Verify that user is able to cancel T1 Type Transit Declaration when Dispatch from EU Country to NonEU Country
            Given I click on manage menu on the application header
            And   I click on transit declarations link
            And   transit declaration page opens up successfully
            And   I click on create declaration button
            And   I enter reference details for Transit declaration
                  | DeclarationType | DeclarationPlace |
                  | T1              | Belfast          |
            And   I click save as draft button
            And   I enter entry reference number in search entry on transit dashboard
            And   I click on Search button on transit dashboard
            And   I click action button
            And   I click cancel button in dropdown
            And   I click delete button on confirmation popup
            And   I enter entry reference number in search entry on transit dashboard
            When  I click on Search button on transit dashboard
            Then  Transit draft declaration should be deleted successfully


