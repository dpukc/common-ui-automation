Feature: Cancel transit draft declaration

      Background: Login to the application
            Given I go to application
            When  I enter valid credentials
            And I click on login button
            Then Application home page opens successfully

      @cancel-transit-draft-declaration @smoke-front-end @transit @T1Declaration @EUToNONEU
      Scenario: Verify that user is able to cancel T1 Type Transit Declaration when Dispatch from EU Country to NonEU Country
            Given I click on manage menu on the application header
            And   I click on transit declarations link
            And   transit declaration page opens up successfully
            And   I click show more button
            And   I select declaration status as "Declaration Accepted"
            And   I click on Search button on transit dashboard
            Then  Transit declartion should be search and have status "Declaration Accepted (MRN allocated)"


